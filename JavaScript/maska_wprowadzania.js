function maska_wprowadzania_function() {
  console.log("Maska_wprowadzania_function");
  tekst = $(this).val();
  typ = $(this).attr("data-type");
  if((typ == "imie/nazwisko") || (typ == "miejsce") || (typ == "ulica")) {
    if(tekst.length == 1) {
      tekst = tekst.toUpperCase();
      $(this).val(tekst);
    }
    if($(this).attr("id") == "miasto_zamieszkania_uczniowie_form")
      $("#miasto_zameldowania_uczniowie_form").val($(this).val());
    else if($(this).attr("id") == "ulica_zamieszkania_uczniowie_form")
      $("#ulica_zameldowania_uczniowie_form").val($(this).val());
    else if($(this).attr("id") == "miasto_zamieszkania_rodzic1_form")
      $("#miasto_zameldowania_rodzic1_form").val($(this).val());
    else if($(this).attr("id") == "miasto_zamieszkania_rodzic2_form")
      $("#miasto_zameldowania_rodzic2_form").val($(this).val());
    else if($(this).attr("id") == "ulica_zamieszkania_rodzic1_form")
      $("#ulica_zameldowania_rodzic1_form").val($(this).val());
    else if($(this).attr("id") == "ulica_zamieszkania_rodzic2_form")
      $("#ulica_zameldowania_rodzic2_form").val($(this).val());
    else if($(this).attr("id") == "miasto_zamieszkania_nauczyciele_form")
      $("#miasto_zameldowania_nauczyciele_form").val($(this).val());
    else if($(this).attr("id") == "ulica_zamieszkania_nauczyciele_form")
      $("#ulica_zameldowania_nauczyciele_form").val($(this).val());

    console.log($(this).val());
  }
  if(typ == "kod_pocztowy") {
    znak = tekst[tekst.length-1];
    znak = parseInt(znak);
    if(isNaN(znak)) {
      tekst = tekst.substring(0, tekst.length-1);
      $(this).val(tekst);
    }
    else {
      if(tekst.length == 2) {
        $(this).val(tekst + "-");
      }
      else if(tekst.length > 6) {
        tekst = tekst.substring(0, tekst.length-1);
        $(this).val(tekst);
      }
    }
  if($(this).attr("id") == "kod_pocztowy_zamieszkania_uczniowie_form")
    $("#kod_pocztowy_zameldowania_uczniowie_form").val($(this).val());
  else if($(this).attr("id") == "kod_pocztowy_zamieszkania_rodzic1_form")
    $("#kod_pocztowy_zameldowania_rodzic1_form").val($(this).val());
  else if($(this).attr("id") == "kod_pocztowy_zamieszkania_rodzic2_form")
    $("#kod_pocztowy_zameldowania_rodzic2_form").val($(this).val());
  else if($(this).attr("id") == "kod_pocztowy_zamieszkania_nauczyciele_form")
    $("#kod_pocztowy_zameldowania_nauczyciele_form").val($(this).val());
}
  if(typ == "komorka") {
    znak = tekst[tekst.length-1];
    znak = parseInt(znak);
    if(isNaN(znak)) {
      tekst = tekst.substring(0, tekst.length-1);
      $(this).val(tekst);
    }
    else {
      if((tekst.length == 3) || (tekst.length == 7)) {
        $(this).val(tekst + "-");
      }
      else if(tekst.length > 11) {
        tekst = tekst.substring(0, tekst.length-1);
        $(this).val(tekst);
      }
    }
  }
  if(typ == "stacjonarny") {
    znak = tekst[tekst.length-1];
    znak = parseInt(znak);
    if(isNaN(znak)) {
      tekst = tekst.substring(0, tekst.length-1);
      $(this).val(tekst);
    }
    else {
      if((tekst.length == 3) || (tekst.length == 6) || (tekst.length == 7)) {
        $(this).val(tekst + "-");
      }
      else if(tekst.length > 9) {
        tekst = tekst.substring(0, tekst.length-1);
        $(this).val(tekst);
      }
    }
  }
  if(typ == "pesel") {
    znak = tekst[tekst.length-1];
    znak = parseInt(znak);
    if((isNaN(znak)) || (tekst.length > 11)) {
      tekst = tekst.substring(0, tekst.length-1);
      $(this).val(tekst);
    }
    if($(this).attr("id") == "pesel_nauczyciele_form") {
      if(tekst.length >= 2) {
        rok = tekst.substring(0,2);
        $("#rok_urodzenia_nauczyciele_form").val(rok);
      }
    }
  }
}
