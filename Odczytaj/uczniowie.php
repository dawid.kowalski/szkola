<?php
  if($_SERVER['REQUEST_METHOD'] != 'POST')
    exit();
  require '../connect.php';
  require '../szyfruj.php';
  $mysqli = new mysqli($host, $user, $password, $database);
  $dane = array();
  $query = "select uczniowie.id, uczniowie.imie, uczniowie.drugie_imie, uczniowie.nazwisko, uczniowie.pesel, uczniowie.data_urodzenia, uczniowie.miejsce_urodzenia, uczniowie.data_orzeczenie, uczniowie.id_zameldowania, uczniowie.id_klasy , uczniowie.id_zamieszkania, uczniowie.id_kontaktu ,uczniowie.samodzielne_poruszanie, uczniowie.archiwum, kontakty.telefon, kontakty.telefon_stacionarny, kontakty.email, szkoly.nazwa, klasy.klasa as klasa , zamieszkanie.miasto as 'zamieszkanie_miasto', zamieszkanie.ulica as 'zamieszkanie_ulica', zamieszkanie.kod_pocztowy as 'zamieszkanie_kod', zameldowanie.miasto as 'zameldowanie_miasto', zameldowanie.ulica as 'zameldowanie_ulica', zameldowanie.kod_pocztowy as 'zameldowanie_kod' from klasy, szkoly, uczniowie, zamieszkanie, zameldowanie, kontakty where uczniowie.id_kontaktu = kontakty.id and klasy.id = uczniowie.id_klasy and klasy.id_szkoly = szkoly.id and uczniowie.id_zamieszkania = zamieszkanie.id and zameldowanie.id = uczniowie.id_zameldowania GROUP by uczniowie.id order by szkoly.nazwa";
  $result = $mysqli->query($query);
  echo $mysqli->error;
  while($row = $result->fetch_assoc()) {
    unset($osoba);
    $osoba["id"] = $row["id"];
    $osoba["imie"] = decrypt($row["imie"]);
    $osoba["drugie_imie"] = decrypt($row["drugie_imie"]);
    $osoba["nazwisko"] = decrypt($row["nazwisko"]);
    $osoba["pesel"] = decrypt($row["pesel"]);
    $osoba["data_urodzenia"] = decrypt($row["data_urodzenia"]);
    $osoba["miejsce_urodzenia"] = decrypt($row["miejsce_urodzenia"]);
    $osoba["data_orzeczenie"] = decrypt($row["data_orzeczenie"]);
    $osoba["samodzielne_poruszanie"] = decrypt($row["samodzielne_poruszanie"]);
    $osoba["archiwum"] = decrypt($row["archiwum"]);
    $osoba["zamieszkanie"]["miasto"] = decrypt($row["zamieszkanie_miasto"]);
    $osoba["zamieszkanie"]["ulica"] = decrypt($row["zamieszkanie_ulica"]);
    $osoba["zamieszkanie"]["kod"] = decrypt($row["zamieszkanie_kod"]);
    $osoba["zameldowanie"]["miasto"] = decrypt($row["zameldowanie_miasto"]);
    $osoba["zameldowanie"]["ulica"] = decrypt($row["zameldowanie_ulica"]);
    $osoba["zameldowanie"]["kod"] = decrypt($row["zameldowanie_kod"]);
    $osoba["telefon"] = decrypt($row["telefon"]);
    $osoba["telefon_stacjonarny"] = decrypt($row["telefon_stacionarny"]);
    $osoba["email"] = decrypt($row["email"]);
    $osoba["szkola"] = decrypt($row["nazwa"]);
    $osoba["klasa"] = $row["klasa"];
    $osoba["id_zameldowania"] = $row["id_zameldowania"];
    $osoba["id_zamieszkania"] = $row["id_zameldowania"];
    $osoba["id_kontaktu"] = $row["id_kontaktu"];
    $osoba["id_klasy"] = $row["id_klasy"];

    $query = "SELECT rodzice.id, rodzice.imie, rodzice.drugie_imie, rodzice.nazwisko, rodzice.id_zameldowania, rodzice.id_zamieszkania, rodzice.id_kontaktu, zamieszkanie.miasto as zamie_miasto, zamieszkanie.ulica as zamie_ulica, zamieszkanie.kod_pocztowy as zamie_kod, zameldowanie.miasto as zamel_miasto, zameldowanie.ulica as zamel_ulica, zameldowanie.kod_pocztowy as zamel_kod, kontakty.telefon, kontakty.telefon_stacionarny, kontakty.email FROM rodzice, zamieszkanie, zameldowanie, kontakty, uczniowie_rodzice where rodzice.id = uczniowie_rodzice.id_rodzica and rodzice.id_zamieszkania = zamieszkanie.id and rodzice.id_zameldowania = zameldowanie.id and rodzice.id_kontaktu = kontakty.id and uczniowie_rodzice.id_ucznia = ".$osoba['id'].";";
    $rodzice = $mysqli->query($query);
    echo $mysqli->error;
    while($rodzic = $rodzice->fetch_assoc()) {
      $rodzic_dane["id"] = $rodzic["id"];
      $rodzic_dane["imie"] = decrypt($rodzic["imie"]);
      $rodzic_dane["drugie_imie"] = decrypt($rodzic["drugie_imie"]);
      $rodzic_dane["nazwisko"] = decrypt($rodzic["nazwisko"]);
      $rodzic_dane["zameldowanie"]["id"] = $rodzic["id_zameldowania"];
      $rodzic_dane["zameldowanie"]["miasto"] = decrypt($rodzic["zamel_miasto"]);
      $rodzic_dane["zameldowanie"]["ulica"] = decrypt($rodzic["zamel_ulica"]);
      $rodzic_dane["zameldowanie"]["kod"] = decrypt($rodzic["zamel_kod"]);
      $rodzic_dane["zamieszkanie"]["id"] = $rodzic["id_zamieszkania"];
      $rodzic_dane["zamieszkanie"]["miasto"] = decrypt($rodzic["zamie_miasto"]);
      $rodzic_dane["zamieszkanie"]["ulica"] = decrypt($rodzic["zamie_ulica"]);
      $rodzic_dane["zamieszkanie"]["kod"] = decrypt($rodzic["zamie_kod"]);
      $rodzic_dane["id_kontaktu"] = $rodzic["id_kontaktu"];
      $rodzic_dane["telefon"] = decrypt($rodzic["telefon"]);
      $rodzic_dane["telefon_stacjonarny"] = decrypt($rodzic["telefon_stacionarny"]);
      $rodzic_dane["email"] = decrypt($rodzic["email"]);
      $osoba["rodzic"][] = $rodzic_dane;
    }
    foreach($osoba["zameldowanie"] as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }
    foreach($osoba["zamieszkanie"] as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }
    foreach($osoba["rodzic"][0]["zameldowanie"] as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }
    foreach($osoba["rodzic"][0]["zamieszkanie"] as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }
    foreach($osoba["rodzic"][1]["zameldowanie"] as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }
    foreach($osoba["rodzic"][1]["zamieszkanie"] as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }
    foreach($osoba["rodzic"] as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }
    foreach($osoba as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }
    $dane[] = $osoba;
  }
  switch ($_POST["archiwum"]) {
    case 'wszyscy':
      $kto = -1;
      break;
    case 'uczen':
      $kto = "false";
      break;
    case 'archiwum':
      $kto = "true";
      break;
  }

  for($i = 0; $i < count($dane); $i++){
    if(($dane[$i]["archiwum"] !== $kto) && ($kto != -1))
      unset($dane[$i]);
    if(!empty($_POST["szukaj"])){
      if(strpos(json_encode($dane[$i]), $_POST["szukaj"]) === false)
        unset($dane[$i]);
    }
  }

  foreach($dane as $key => &$value) {
    $nazwiska[] = $value["nazwisko"];
    $klasy[] = $value["klasa"];
  }

  if(count($dane) > 0) {
    switch($_POST["sortuj"]) {
      case 'a->z':
        array_multisort($dane, SORT_ASC, $nazwiska);
        break;
      case 'z->a':
        array_multisort($dane, SORT_ASC, $nazwiska);
        $dane = array_reverse($dane);
        break;
        case 'n->1':
          array_multisort($dane, SORT_ASC, $klasy);
          break;
        case '1->n':
          array_multisort($dane, SORT_ASC, $klasy);
          $dane = array_reverse($dane);
          break;
    }
  }
  echo json_encode($dane);
?>
