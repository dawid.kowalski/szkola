
function pobierz_uczniowie_lista_function() {
  console.log("pobierz_uczniowie_lista_function");
  $.ajax({
    async: false,
    url: "Odczytaj/uczniowie.php",
    method: "post",
    data: {
      archiwum: $("#archiwum_uczniowie_lista :selected").attr("value"),
      sortuj: $("#sortowanie_uczniowie_lista :selected").attr("value"),
      szukaj: $("#szukaj_uczniowie_lista").val()
    }
  })
  .done(res => {
    console.log(res);
    if(res != "null") {
      uczniowie = JSON.parse(res);
      wyswietl_uczniowie_lista_function(uczniowie);
    }
    else {
      $("#lista_uczniowie_div").empty();
    }
  })
  .fail(error => {
    console.log(error);
  });
}

function pobierz_nauczyciele_lista_function() {
  console.log("pobierz_nauczyciele_lista_function");
  $.ajax({
    async: "false",
    method: "post",
    url: "Odczytaj/nauczyciele.php",
    data: {
      archiwum: $("#archiwum_nauczyciele_lista :selected").attr("value"),
      sortuj: $("#sortowanie_nauczyciele_lista :selected").attr("value"),
      szukaj: $("#szukaj_nauczyciele_lista").val()
    }
  })
  .done(res => {
    console.log(res);
    if(res != "null") {
      nauczyciele = JSON.parse(res);
      wyswietl_nauczyciele_lista_function(nauczyciele);
    }
  })
  .fail(error => {
    console.error(error);
  })
}

function pobierz_przedmioty_lista_function() {
    $.ajax({
      async: false,
      url: "Odczytaj/przedmioty.php",
      method: "post"
    })
    .done(res => {
      przedmiot = JSON.parse(res);
    })
    .fail(error => {
      console.error(error);
    });
    return przedmiot;
  }

function pobierz_szkoly_lista_function() {
  $.ajax({
    async: false,
    url: "Odczytaj/szkoly_klasy.php",
    method: "post"
  })
  .done(res => {
    szkoly = JSON.parse(res);
  })
  .fail(error => {
    console.error(error);
  });
  return szkoly;
}
