<?php
  require '../connect.php';
  require '../szyfruj.php';
  $mysqli = new mysqli($host, $user, $password, $database);

  $imie = htmlspecialchars($_POST["imie"]);
  $imie = $mysqli->real_escape_string($imie);
  $imie = encrypt($imie);

  $drugie_imie = htmlspecialchars($_POST["drugie_imie"]);
  $drugie_imie = $mysqli->real_escape_string($drugie_imie);
  $drugie_imie = encrypt($drugie_imie);

  $nazwisko = htmlspecialchars($_POST["nazwisko"]);
  $nazwisko = $mysqli->real_escape_string($nazwisko);
  $nazwisko = encrypt($nazwisko);

  $pesel = htmlspecialchars($_POST["pesel"]);
  $pesel = $mysqli->real_escape_string($pesel);
  $pesel = encrypt($pesel);

  $rok_urodzenia = htmlspecialchars($_POST["rok_urodzenia"]);
  $rok_urodzenia = $mysqli->real_escape_string($rok_urodzenia);
  $rok_urodzenia = encrypt($rok_urodzenia);

  $archiwum = htmlspecialchars("false");
  $archiwum = $mysqli->real_escape_string($archiwum);
  $archiwum = encrypt($archiwum);

  $zameldowanie["miasto"] = htmlspecialchars($_POST["zameldowanie"]["miasto"]);
  $zameldowanie["miasto"] = $mysqli->real_escape_string($zameldowanie["miasto"]);
  $zameldowanie["miasto"] = encrypt($zameldowanie["miasto"]);

  $zameldowanie["ulica"] = htmlspecialchars($_POST["zameldowanie"]["ulica"]);
  $zameldowanie["ulica"] = $mysqli->real_escape_string($zameldowanie["ulica"]);
  $zameldowanie["ulica"] = encrypt($zameldowanie["ulica"]);

  $zameldowanie["kod"] = htmlspecialchars($_POST["zameldowanie"]["kod"]);
  $zameldowanie["kod"] = $mysqli->real_escape_string($zameldowanie["kod"]);
  $zameldowanie["kod"] = encrypt($zameldowanie["kod"]);

  $zamieszkanie["miasto"] = htmlspecialchars($_POST["zamieszkanie"]["miasto"]);
  $zamieszkanie["miasto"] = $mysqli->real_escape_string($zamieszkanie["miasto"]);
  $zamieszkanie["miasto"] = encrypt($zamieszkanie["miasto"]);

  $zamieszkanie["ulica"] = htmlspecialchars($_POST["zamieszkanie"]["ulica"]);
  $zamieszkanie["ulica"] = $mysqli->real_escape_string($zamieszkanie["ulica"]);
  $zamieszkanie["ulica"] = encrypt($zamieszkanie["ulica"]);

  $zamieszkanie["kod"] = htmlspecialchars($_POST["zamieszkanie"]["kod"]);
  $zamieszkanie["kod"] = $mysqli->real_escape_string($zamieszkanie["kod"]);
  $zamieszkanie["kod"] = encrypt($zamieszkanie["kod"]);

  $kontakt["telefon"] = htmlspecialchars($_POST["kontakt"]["telefon"]);
  $kontakt["telefon"] = $mysqli->real_escape_string($kontakt["telefon"]);
  $kontakt["telefon"] = encrypt($kontakt["telefon"]);

  $kontakt["stacjonarny"] = htmlspecialchars($_POST["kontakt"]["stacjonarny"]);
  $kontakt["stacjonarny"] = $mysqli->real_escape_string($kontakt["stacjonarny"]);
  $kontakt["stacjonarny"] = encrypt($kontakt["stacjonarny"]);

  $kontakt["email"] = htmlspecialchars($_POST["kontakt"]["email"]);
  $kontakt["email"] = $mysqli->real_escape_string($kontakt["email"]);
  $kontakt["email"] = encrypt($kontakt["email"]);

  $query = "insert into kontakty values(null, '".$kontakt["telefon"]."', '".$kontakt["stacjonarny"]."', '".$kontakt["email"]."');";
  $mysqli->query($query);
  $id_kontaktu = $mysqli->insert_id;
  echo $mysqli->error;

  $query = "insert into zamieszkanie values(null, '".$zamieszkanie["miasto"]."', '".$zamieszkanie["ulica"]."', '".$zamieszkanie["kod"]."');";
  $mysqli->query($query);
  $id_zamieszkanie = $mysqli->insert_id;
  echo $mysqli->error;

  $query = "insert into zameldowanie values(null, '".$zameldowanie["miasto"]."', '".$zameldowanie["ulica"]."', '".$zameldowanie["kod"]."');";
  $mysqli->query($query);
  $id_zameldowanie = $mysqli->insert_id;
  echo $mysqli->error;

  $query = "insert into nauczyciele values(null, '".$imie."', '".$drugie_imie."', '".$nazwisko."', '".$pesel."', '".$rok_urodzenia."', $id_zamieszkanie, $id_zameldowanie, $id_kontaktu, '$archiwum');";
  $mysqli->query($query);
  echo $mysqli->error;
  $id_nauczyciela = $mysqli->insert_id;
  foreach ($_POST["przedmioty"] as $id_przedmiotu) {
    $query = "insert into uczone_przedmioty values(null, $id_nauczyciela, $id_przedmiotu)";
    $mysqli->query($query);
    echo $mysqli->error;
  }
  foreach ($_POST["studia"] as $studia) {
    $studia["uczelnia"] = htmlspecialchars($studia["uczelnia"]);
    $studia["uczelnia"] = $mysqli->real_escape_string($studia["uczelnia"]);
    $studia["uczelnia"] = encrypt($studia["uczelnia"]);

    $studia["kierunek"] = htmlspecialchars($studia["kierunek"]);
    $studia["kierunek"] = $mysqli->real_escape_string($studia["kierunek"]);
    $studia["kierunek"] = encrypt($studia["kierunek"]);

    $studia["data"] = htmlspecialchars($studia["data"]);
    $studia["data"] = $mysqli->real_escape_string($studia["data"]);
    $studia["data"] = encrypt($studia["data"]);

    $studia["stopien"] = htmlspecialchars($studia["stopien"]);
    $studia["stopien"] = $mysqli->real_escape_string($studia["stopien"]);
    $studia["stopien"] = encrypt($studia["stopien"]);

    $query = "insert into studia values(null, $id_nauczyciela, '".$studia["uczelnia"]."', '".$studia["kierunek"]."', '".$studia["data"]."', '".$studia["stopien"]."')";
    $mysqli->query($query);
    echo $mysqli->error;
  }
?>
