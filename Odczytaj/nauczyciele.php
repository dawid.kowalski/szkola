<?php
  if($_SERVER['REQUEST_METHOD'] != 'POST')
    exit();
  require '../connect.php';
  require '../szyfruj.php';

  $dane = array();
  $nazwiska = array();
  $mysqli = new mysqli($host, $user, $password, $database);
  $query = "select nauczyciele.id, nauczyciele.imie, nauczyciele.drugie_imie, nauczyciele.nazwisko, nauczyciele.pesel, nauczyciele.rok_urodzenia, nauczyciele.id_zameldowania, nauczyciele.id_zamieszkania, nauczyciele.id_kontaktu, nauczyciele.archiwum, kontakty.telefon, kontakty.telefon_stacionarny, kontakty.email, zamieszkanie.miasto as 'zamieszkanie_miasto', zamieszkanie.ulica as 'zamieszkanie_ulica', zamieszkanie.kod_pocztowy as 'zamieszkanie_kod', zameldowanie.miasto as 'zameldowanie_miasto', zameldowanie.ulica as 'zameldowanie_ulica', zameldowanie.kod_pocztowy as 'zameldowanie_kod' from zamieszkanie, zameldowanie, kontakty, nauczyciele where nauczyciele.id_kontaktu = kontakty.id and nauczyciele.id_zamieszkania = zamieszkanie.id and zameldowanie.id = nauczyciele.id_zameldowania GROUP by nauczyciele.id";
  $result = $mysqli->query($query);
  echo $mysqli->error;
  while($row = $result->fetch_assoc()) {
    unset($osoba);
    $osoba["id"] = $row["id"];
    $osoba["imie"] = decrypt($row["imie"]);
    $osoba["drugie_imie"] = decrypt($row["drugie_imie"]);
    $osoba["nazwisko"] = decrypt($row["nazwisko"]);
    $nazwiska[] = $osoba["nazwisko"];
    $osoba["pesel"] = decrypt($row["pesel"]);
    $osoba["rok_urodzenia"] = decrypt($row["rok_urodzenia"]);
    $osoba["archiwum"] = decrypt($row["archiwum"]);

    $osoba["zamieszkanie"]["miasto"] = decrypt($row["zamieszkanie_miasto"]);
    $osoba["zamieszkanie"]["ulica"] = decrypt($row["zamieszkanie_ulica"]);
    $osoba["zamieszkanie"]["kod"] = decrypt($row["zamieszkanie_kod"]);

    $osoba["zameldowanie"]["miasto"] = decrypt($row["zameldowanie_miasto"]);
    $osoba["zameldowanie"]["ulica"] = decrypt($row["zameldowanie_ulica"]);
    $osoba["zameldowanie"]["kod"] = decrypt($row["zameldowanie_kod"]);

    $osoba["kontakt"]["telefon"] = decrypt($row["telefon"]);
    $osoba["kontakt"]["telefon_stacjonarny"] = decrypt($row["telefon_stacionarny"]);
    $osoba["kontakt"]["email"] = decrypt($row["email"]);

    $osoba["id_zameldowania"] = $row["id_zameldowania"];
    $osoba["id_zamieszkania"] = $row["id_zameldowania"];
    $osoba["id_kontaktu"] = $row["id_kontaktu"];

    $studia_query = "select studia.id, studia.uczelnia, studia.kierunek, studia.data_ukonczenia, studia.stopien from studia where id_nauczyciela = '".$osoba["id"]."'";
    $studia_result = $mysqli->query($studia_query);
    $studia = array();
    while($studia_row = $studia_result->fetch_assoc()) {
      $tmp = array();
      $tmp["id"] = $studia_row["id"];

      $tmp["uczelnia"] = decrypt($studia_row["uczelnia"]);
      $tmp["uczelnia"] = str_replace("\0", '', $tmp["uczelnia"]);

      $tmp["kierunek"] = decrypt($studia_row["kierunek"]);
      $tmp["kierunek"] = str_replace("\0", '', $tmp["kierunek"]);

      $tmp["data_ukonczenia"] = decrypt($studia_row["data_ukonczenia"]);
      $tmp["data_ukonczenia"] = str_replace("\0", '', $tmp["data_ukonczenia"]);

      $tmp["stopien"] = decrypt($studia_row["stopien"]);
      $tmp["stopien"] = str_replace("\0", '', $tmp["stopien"]);
      $studia[] = $tmp;
    }
    $osoba["studia"] = $studia;
    $uczone_przedmioty = "select przedmioty.nazwa from przedmioty, uczone_przedmioty where uczone_przedmioty.id_nauczyciela = '".$osoba["id"]."' and uczone_przedmioty.id_przedmiotu = przedmioty.id";
    $przedmioty_result = $mysqli->query($uczone_przedmioty);
    $osoba["przedmioty"] = array();
    while($przedmiot = $przedmioty_result->fetch_assoc()) {
      $osoba["przedmioty"][] = decrypt($przedmiot["nazwa"]);
    }

    foreach($osoba["zameldowanie"] as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }
    foreach($osoba["zamieszkanie"] as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }

    foreach($osoba as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }

    foreach ($osoba["przedmioty"] as &$value) {
      $value = str_replace("\0", '', $value);
      $prze[] = $value;
    }

    $dane[] = $osoba;
  }
  switch ($_POST["archiwum"]) {
    case 'wszyscy':
      $kto = -1;
      break;
    case 'nauczyciel':
      $kto = "false";
      break;
    case 'archiwum':
      $kto = "true";
      break;
  }
  if(count($dane) > 0) {
    for($i = 0; $i < count($dane); $i++){
      if(($dane[$i]["archiwum"] !== $kto) && ($kto != -1))
        unset($dane[$i]);
      if(!empty($_POST["szukaj"])){
        if(strpos(strtolower(json_encode($dane[$i])), strtolower($_POST["szukaj"])) === false)
          unset($dane[$i]);
      }
    }
    if(count($dane) > 0)
    switch($_POST["sortuj"]) {
      case 'nazw a->z':
        array_multisort($dane, SORT_ASC, $nazwiska);
        break;
      case 'nazw z->a':
        array_multisort($dane, SORT_DESC, $nazwiska);
        break;
      case 'prze a->z':
        array_multisort($dane, SORT_ASC, $prze);
        break;
      case 'prze z->a':
        array_multisort($dane, SORT_DESC, $prze);
        break;
    }
  }
    if(isSet($dane))
      echo json_encode($dane);
    else {
      $dane = null;
      echo json_encode($dane);
    }

?>
