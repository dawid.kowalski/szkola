<?php
// Include the main TCPDF lihrary (search for installation path).
// require_once('tcpdf_include.php');
require_once('tcpdf.php');

// create new PDF document
$pdf = new TCPDF("LANDSCAPE", PDF_UNIT, "a4", true, 'UTF-8', false);

// set document information
// $pdf->SetCreator(PDF_CREATOR);
// $pdf->SetAuthor('Nicola Asuni');
// $pdf->SetTitle('TCPDF Example 001');
// $pdf->SetSubject('TCPDF Tutorial');
// $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page hreaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
// Set some content to print
$html = $_POST["tabela"];

// // Print text using writeHTMLCell()
$pdf->writeHTML($html, true, false, false, false, '');




// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
 $nazwa = "Raport/PDF/".date("d-m-Y");
 $nazwa .= " Nauczyciel.pdf";
 $pdf->Output("/var/www/html/szkola/".$nazwa, 'F');

 echo $nazwa;

//============================================================+
// END OF FILE
//============================================================+
