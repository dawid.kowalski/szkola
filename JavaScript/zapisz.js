function dodaj_przedmiot_function() {
  console.log("dodaj_przedmiot_function");
    if(($("#nazwa_przedmiotu_form").val().length > 0)) {
    json = {
      skrot: $("#skrot_przedmiotu_form").val(),
      nazwa: $("#nazwa_przedmiotu_form").val()
    }
    $.ajax({
      url: "Zapisz/przedmiot.php".val(),
      method: "post".val(),
      data: json
    })
    .done(res => {
      $("#skrot_przedmiotu_form").val("");
      $("#nazwa_przedmiotu_form").val("");
      $("#informacje").html("Przedmiot został dodany");
    })
    .fail(error => {
      console.log(error);
      $("#informacje").html("Nie udało się zapisać przedmiotu. \n Spróbuje jeszcze raz");
    });
  }
  else {
    $("#informacje").html("Musisz podać nazwę przedmiotu!");
  }
}

function dodaj_szkole_function() {
  console.log("dodaj_szkole_function");
    if(($("#nazwa_szkoly_form").val().length > 0) && ($("#ilosc_szkoly_form").val().length > 0)) {
    json = {
      nazwa: $("#nazwa_szkoly_form").val(),
      ilosc: $("#ilosc_szkoly_form").val()
    }

    $.ajax({
      url: "Zapisz/szkoly_klasy.php",
      method: "post",
      data: json
    })
    .done(res => {
      if(res == 1062) /*duplicat_error*/ {
        $("#informacje").html("Taka szkoła już istnieje!");
      }
      else {
        console.log(res);
        $("#nazwa_szkoly_form").val("");
        $("#ilosc_szkoly_form").val("");
        $("#informacje").html("Szkoła została dodana");
      }
    })
    .fail(error => {
      console.log(error);
      $("#informacje").html("Nie udało się zapisać szkoły. \n Spróbuje jeszcze raz");
    });
  }
  else {
    $("#informacje").html("Musisz podać nazwę szkoły oraz liczbę klas");
  }
}

function dodaj_ucznia_function(event) {
  console.log("dodaj_ucznia_function");
  json = {
    imie: $("#imie_uczniowie_form").val(),
    drugie_imie: $("#drugie_imie_uczniowie_form").val(),
    nazwisko: $("#nazwisko_uczniowie_form").val(),
    data_urodzenia: $("#data_urodzenia_uczniowie_form").val(),
    miejsce_urodzenia: $("#miejsce_urodzenia_uczniowie_form").val(),
    data_orzeczenia: $("#data_orzeczenia_uczniowie_form").val(),
    samodzielne_poruszanie: $("#samodzielne_poruszanie_uczniowie_form").prop("checked").val(),
    miasto_zameldowanie: $("#miasto_zameldowania_uczniowie_form").val(),
    ulica_zameldowanie: $("#ulica_zameldowania_uczniowie_form").val(),
    kod_pocztowy_zameldowanie: $("#kod_pocztowy_zameldowania_uczniowie_form").val(),
    miasto_zamieszkania: $("#miasto_zamieszkania_uczniowie_form").val(),
    ulica_zamieszkania: $("#ulica_zamieszkania_uczniowie_form").val(),
    kod_pocztowy_zamieszkania: $("#kod_pocztowy_zamieszkania_uczniowie_form").val(),
    komorka: $("#telefon_uczniowie_form").val(),
    stacjonarny: $("#telefon_stacjonarny_uczniowie_form").val(),
    email: $("#email_uczniowie_form").val(),
    szkola: $("#szkola_uczniowie_form option:selected").text().val(),
    klasa: $("#klasa_uczniowie_form option:selected").text().val(),
    pesel: $("#pesel_uczniowie_form").val(),

    imie_rodzic1: $("#imie_rodzic1_form").val(),
    drugie_imie_rodzic1: $("#drugie_imie_rodzic1_form").val(),
    nazwisko_rodzic1: $("#nazwisko_rodzic1_form").val(),
    miasto_zamieszkania_rodzic1: $("#miasto_zamieszkania_rodzic1_form").val(),
    ulica_zamieszkania_rodzic1: $("#ulica_zamieszkania_rodzic1_form").val(),
    kod_pocztowy_zamieszkania_rodzic1: $("#kod_pocztowy_zamieszkania_rodzic1_form").val(),
    miasto_zameldowanie_rodzic1: $("#miasto_zameldowania_rodzic1_form").val(),
    ulica_zameldowanie_rodzic1: $("#ulica_zameldowania_rodzic1_form").val(),
    kod_pocztowy_zameldowanie_rodzic1: $("#kod_pocztowy_zameldowania_rodzic1_form").val(),
    telefon_rodzic1: $("#telefon_rodzic1_form").val(),
    telefon_stacjonarny_rodzic1: $("#telefon_stacjonarny_rodzic1_form").val(),
    email_rodzic1: $("#email_rodzic1_form").val(),

    imie_rodzic2: $("#imie_rodzic2_form").val(),
    drugie_imie_rodzic2: $("#drugie_imie_rodzic2_form").val(),
    nazwisko_rodzic2: $("#nazwisko_rodzic2_form").val(),
    miasto_zamieszkania_rodzic2: $("#miasto_zamieszkania_rodzic2_form").val(),
    ulica_zamieszkania_rodzic2: $("#ulica_zamieszkania_rodzic2_form").val(),
    kod_pocztowy_zamieszkania_rodzic2: $("#kod_pocztowy_zamieszkania_rodzic2_form").val(),
    miasto_zameldowania_rodzic2: $("#miasto_zameldowania_rodzic2_form").val(),
    ulica_zameldowania_rodzic2: $("#ulica_zameldowania_rodzic2_form").val(),
    kod_pocztowy_zameldowania_rodzic2: $("#kod_pocztowy_zameldowania_rodzic2_form").val(),
    telefon_rodzic2: $("#telefon_rodzic2_form").val(),
    telefon_stacjonarny_rodzic2: $("#telefon_stacjonarny_rodzic2_form").val(),
    email_rodzic2: $("#email_rodzic2_form").val()
  }
  console.log(json);
  if(walidator_form_function('uczen')) {
    $.ajax({
      url: "Zapisz/uczen.php",
      method: "post",
      data: json
    })
    .done(res => {
      $("#informacje").html("Uczeń został dodany");
      console.log(res);
    })
    .fail(error => {
      console.log(error);
    });
  }
}

function dodaj_nauczyciela_function() {
  console.log("dodaj_nauczyciela_function");
  var dane_studia = Array();
  for(i = 0; i < $(".studia_uczelnia_nauczyciele_form").length; i++) {
    dane_studia[i] = {};
    dane_studia[i]["uczelnia"] = $(".studia_uczelnia_nauczyciele_form").eq(i).val();
    dane_studia[i]["kierunek"] = $(".studia_kierunek_nauczyciele_form").eq(i).val();
    dane_studia[i]["data"] = $(".studia_data_ukonczenia_nauczyciele_form").eq(i).val();
    dane_studia[i]["stopien"] = $(".studia_stopien_nauczyciele_form :checked").eq(i).text();
  }
  json = {
    imie: $("#imie_nauczyciele_form").val(),
    drugie_imie: $("#drugie_imie_nauczyciele_form").val(),
    nazwisko: $("#nazwisko_nauczyciele_form").val(),
    pesel: $("#pesel_nauczyciele_form").val(),
    rok_urodzenia: $("#rok_urodzenia_nauczyciele_form").val(),
    zamieszkanie: {
      miasto: $("#miasto_zamieszkania_nauczyciele_form").val(),
      ulica: $("#ulica_zamieszkania_nauczyciele_form").val(),
      kod: $("#kod_pocztowy_zamieszkania_nauczyciele_form").val(),
    },
    zameldowanie: {
      miasto: $("#miasto_zameldowania_nauczyciele_form").val(),
      ulica: $("#ulica_zameldowania_nauczyciele_form").val(),
      kod: $("#kod_pocztowy_zameldowania_nauczyciele_form").val(),
    },
    kontakt: {
      telefon: $("#telefon_nauczyciele_form").val(),
      stacjonarny: $("#telefon_stacjonarny_nauczyciele_form").val(),
      email: $("#email_nauczyciele_form").val(),
    },
    studia: dane_studia,
    przedmioty: $("#uczone_przedmioty_nauczyciele_form").val(),
  }
  console.log(json);
    $.ajax({
      url: "Zapisz/nauczyciel.php",
      method: "post",
      data: json
    })
    .done(res => {
      $("#informacje").html("Nauczyciel został dodany");
      console.log(res);
      Nauczyciele_function();
    })
    .fail(error => {
      console.log(error);
    });
}
