<?php
  if($_SERVER['REQUEST_METHOD'] != 'POST')
    exit();
  require '../connect.php';
  require '../szyfruj.php';

  if(!isset($_POST["sortuj"])){
    $_POST["sortuj"] = "nazwisko";
    $domyslny = true;
  }
  if(isset($_POST["przedmioty"]))
    $szukany_przedmiot = "and uczone_przedmioty.id_przedmiotu = ".$_POST["przedmioty"][0];
  else
    $szukany_przedmiot = "";
  $dane = array();
  $mysqli = new mysqli($host, $user, $password, $database);
  $widoczne = $_POST["widoczne"];
  $query = "select nauczyciele.id, nauczyciele.imie, nauczyciele.drugie_imie, nauczyciele.nazwisko, nauczyciele.pesel, nauczyciele.rok_urodzenia, nauczyciele.id_zameldowania, nauczyciele.id_zamieszkania, nauczyciele.id_kontaktu, nauczyciele.archiwum, kontakty.telefon, kontakty.telefon_stacionarny, kontakty.email, zamieszkanie.miasto as 'zamieszkanie_miasto', zamieszkanie.ulica as 'zamieszkanie_ulica', zamieszkanie.kod_pocztowy as 'zamieszkanie_kod', zameldowanie.miasto as 'zameldowanie_miasto', zameldowanie.ulica as 'zameldowanie_ulica', zameldowanie.kod_pocztowy as 'zameldowanie_kod' from zamieszkanie, zameldowanie, kontakty, nauczyciele, przedmioty, uczone_przedmioty where nauczyciele.id_kontaktu = kontakty.id and nauczyciele.id_zamieszkania = zamieszkanie.id and zameldowanie.id = nauczyciele.id_zameldowania $szukany_przedmiot and uczone_przedmioty.id_nauczyciela = nauczyciele.id and uczone_przedmioty.id_przedmiotu = przedmioty.id GROUP by nauczyciele.id";
  $result = $mysqli->query($query);
  echo $mysqli->error;
  while($row = $result->fetch_assoc()) {
    unset($osoba);
    $osoba["id"] = $row["id"];
    if(in_array("imie", $widoczne) || ($_POST["sortuj"] == "imie")) $osoba["imie"] = decrypt($row["imie"]);
    if(in_array("drugie_imie", $widoczne) || ($_POST["sortuj"] == "drugie_imie")) $osoba["drugie_imie"] = decrypt($row["drugie_imie"]);
    if(in_array("nazwisko", $widoczne) || ($_POST["sortuj"] == "nazwisko")) $osoba["nazwisko"] = decrypt($row["nazwisko"]);
    if(in_array("pesel", $widoczne) || ($_POST["sortuj"] == "pesel")) $osoba["pesel"] = decrypt($row["pesel"]);
    if(in_array("rok_urodzenia", $widoczne) || ($_POST["sortuj"] == "rok_urodzenia")) $osoba["rok_urodzenia"] = decrypt($row["rok_urodzenia"]);
    if(in_array("archiwum", $widoczne) || ($_POST["sortuj"] == "archiwum") || $_POST["archiwum"] != "BRAK")
      if(decrypt($row["archiwum"]) == true) $osoba["archiwum"] = "TAK";
      else $osoba["archiwum"] = "NIE";

    if(in_array("miejsce_zamieszkania", $widoczne) || ($_POST["sortuj"] == "miejsce_zamieszkania")) {
      $miasto = decrypt($row["zamieszkanie_miasto"]);
      $ulica = decrypt($row["zamieszkanie_ulica"]);
      $kod = decrypt($row["zamieszkanie_kod"]);
      $osoba["miejsce_zamieszkania"] = "$miasto <br> $uloca <br> $kod";
      $osoba["miejsce_zamieszkania"] = str_replace("\0", '', $osoba["miejsce_zamieszkania"]);
    }
    if(in_array("miejsce_zameldowania", $widoczne) || ($_POST["sortuj"] == "miejsce_zameldowania")) {
      $miasto = decrypt($row["zameldowanie_miasto"]);
      $uloca = decrypt($row["zameldowanie_ulica"]);
      $kod = decrypt($row["zameldowanie_kod"]);
      $osoba["miejsce_zameldowania"] = "$miasto <br> $uloca <br> $kod";
      $osoba["miejsce_zameldowania"] = str_replace("\0", '', $osoba["miejsce_zameldowania"]);
    }
    if(in_array("telefon_komorkowy", $widoczne) || ($_POST["sortuj"] == "telefon_komorkowy")) $osoba["telefon_komorkowy"] = decrypt($row["telefon"]);
    if(in_array("telefon_stacionarny", $widoczne) || ($_POST["sortuj"] == "telefon_stacjonarny")) $osoba["telefon_stacjonarny"] = decrypt($row["telefon_stacionarny"]);
    if(in_array("email", $widoczne) || ($_POST["sortuj"] == "email")) $osoba["email"] = decrypt($row["email"]);

    if(in_array("studia", $widoczne) || ($_POST["sortuj"] == "studia")) {
      $studia_query = "select studia.id, studia.uczelnia, studia.kierunek, studia.data_ukonczenia, studia.stopien from studia where id_nauczyciela = '".$osoba["id"]."'";
      $studia_result = $mysqli->query($studia_query);
      $studia = array();
      while($studia_row = $studia_result->fetch_assoc()) {
        $tmp = array();
        $tmp["id"] = $studia_row["id"];

        $tmp["uczelnia"] = decrypt($studia_row["uczelnia"]);
        $tmp["uczelnia"] = str_replace("\0", '', $tmp["uczelnia"]);

        $tmp["kierunek"] = decrypt($studia_row["kierunek"]);
        $tmp["kierunek"] = str_replace("\0", '', $tmp["kierunek"]);

        $tmp["data_ukonczenia"] = decrypt($studia_row["data_ukonczenia"]);
        $tmp["data_ukonczenia"] = str_replace("\0", '', $tmp["data_ukonczenia"]);

        $tmp["stopien"] = decrypt($studia_row["stopien"]);
        $tmp["stopien"] = str_replace("\0", '', $tmp["stopien"]);
        $studia[] = $tmp;
      }

      $osoba["studia"] = $studia;
    }

    if(in_array("przedmioty", $widoczne) || ($_POST["sortuj"] == "przedmioty")) {
      $uczone_przedmioty = "select przedmioty.id, przedmioty.nazwa from przedmioty, uczone_przedmioty where uczone_przedmioty.id_nauczyciela = '".$osoba["id"]."' and uczone_przedmioty.id_przedmiotu = przedmioty.id";
      $przedmioty_result = $mysqli->query($uczone_przedmioty);
      $osoba["przedmioty"] = array();

      while($przedmiot = $przedmioty_result->fetch_assoc()) {
        $osoba["przedmioty"] .= decrypt($przedmiot["nazwa"]);
        $osoba["przedmioty"] .= "</br>";
      }
      foreach ($osoba["przedmioty"] as &$value) {
        $value = str_replace("\0", '', $value);
      }
    }


    foreach($osoba as $key => &$value) {
      $value = str_replace("\0", '', $value);
    }
    unset($osoba["id"]);
    if($_POST["archiwum"] != "BRAK"){
      if($_POST["archiwum"] == $osoba["archiwum"])
        $dane[] = $osoba;
    }
    else $dane[] = $osoba;

  }

  if(count($dane) > 0) {
    uasort($dane, 'porownaj');
    if(!in_array($_POST["sortuj"], $widoczne)){
      for($i = 0; $i < count($dane); $i++) {
        unset($dane[$i][$_POST["sortuj"]]);
      }
    }
  }
  else
    $dane = null;


  echo json_encode($dane);

  function porownaj($element1, $element2) {
    return strcmp($element1[$_POST["sortuj"]], $element2[$_POST["sortuj"]]);
  }

?>
