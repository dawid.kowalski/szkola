<?php
  require '../connect.php';
  require '../szyfruj.php';
  $mysqli = new mysqli($host, $user, $password, $database);

  foreach($_POST as $klucz => $dane) {
    if($klucz != "klasa") {
      $dane = htmlspecialchars($dane);
      $dane = $mysqli->real_escape_string($dane);
      $dane = encrypt($dane);
      $_POST[$klucz] = $dane;
    }
  }
  $imie = $_POST["imie"];
  $drugie_imie = $_POST["drugie_imie"];
  $nazwisko = $_POST["nazwisko"];
  $data_urodzenia = $_POST["data_urodzenia"];
  $miejsce_urodzenia = $_POST["miejsce_urodzenia"];
  $data_orzeczenia = $_POST["data_orzeczenia"];
  $samodzielne_poruszanie = $_POST["samodzielne_poruszanie"];
  $miasto_zameldowanie = $_POST["miasto_zameldowanie"];
  $miasto_zamieszkania = $_POST["miasto_zamieszkania"];
  $ulica_zamieszkania = $_POST["ulica_zamieszkania"];
  $ulica_zameldowanie = $_POST["ulica_zameldowanie"];
  $kod_pocztowy_zameldowanie = $_POST["kod_pocztowy_zameldowanie"];
  $kod_pocztowy_zamieszkania = $_POST["kod_pocztowy_zamieszkania"];
  $komorka = $_POST["komorka"];
  $stacjonarny = $_POST["stacjonarny"];
  $email = $_POST["email"];
  $szkola = $_POST["szkola"];
  $klasa = $_POST["klasa"];
  $pesel = $_POST["pesel"];
  $archiwum = encrypt("false");

  $rodzic[0]["imie"] = $_POST["imie_rodzic1"];
  $rodzic[0]["drugie_imie"] = $_POST["drugie_imie_rodzic1"];
  $rodzic[0]["nazwisko"] = $_POST["nazwisko_rodzic1"];
  $rodzic[0]["zameldowanie"]["miasto"] = $_POST["miasto_zameldowanie_rodzic1"];
  $rodzic[0]["zameldowanie"]["ulica"] = $_POST["ulica_zameldowanie_rodzic1"];
  $rodzic[0]["zameldowanie"]["kod"] = $_POST["kod_pocztowy_zameldowanie_rodzic1"];
  $rodzic[0]["zamieszkania"]["miasto"] = $_POST["miasto_zamieszkania_rodzic1"];
  $rodzic[0]["zamieszkania"]["ulica"] = $_POST["ulica_zamieszkania_rodzic1"];
  $rodzic[0]["zamieszkania"]["kod"] = $_POST["kod_pocztowy_zamieszkania_rodzic1"];
  $rodzic[0]["telefon"] = $_POST["telefon_rodzic1"];
  $rodzic[0]["telefon_stacjonarny"] = $_POST["telefon_stacjonarny_rodzic1"];
  $rodzic[0]["email"] = $_POST["email_rodzic1"];

  $rodzic[1]["imie"] = $_POST["imie_rodzic2"];
  $rodzic[1]["drugie_imie"] = $_POST["drugie_imie_rodzic2"];
  $rodzic[1]["nazwisko"] = $_POST["nazwisko_rodzic2"];
  $rodzic[1]["zameldowanie"]["miasto"] = $_POST["miasto_zameldowania_rodzic2"];
  $rodzic[1]["zameldowanie"]["ulica"] = $_POST["ulica_zameldowania_rodzic2"];
  $rodzic[1]["zameldowanie"]["kod"] = $_POST["kod_pocztowy_zameldowania_rodzic2"];
  $rodzic[1]["zamieszkania"]["miasto"] = $_POST["miasto_zamieszkania_rodzic2"];
  $rodzic[1]["zamieszkania"]["ulica"] = $_POST["ulica_zamieszkania_rodzic2"];
  $rodzic[1]["zamieszkania"]["kod"] = $_POST["kod_pocztowy_zamieszkania_rodzic2"];
  $rodzic[1]["telefon"] = $_POST["telefon_rodzic2"];
  $rodzic[1]["telefon_stacjonarny"] = $_POST["telefon_stacjonarny_rodzic2"];
  $rodzic[1]["email"] = $_POST["email_rodzic2"];

  $query = "SELECT klasy.id as id, szkoly.nazwa as nazwa from klasy, szkoly where  klasy.klasa = '$klasa' and szkoly.id = klasy.id_szkoly";
  $result = $mysqli->query($query);
  while($row = $result->fetch_assoc()) {
    if(decrypt($row["nazwa"]) == decrypt($szkola ))
      $id_klasy = $row["id"];

  }
  $info["error_idklasy"] = $mysqli->error;
  $info["id_klasy"] = $id_klasy;
  $info["klasa_query"] = $query;

  $query = "insert into zameldowanie(miasto, ulica, kod_pocztowy) values('$miasto_zameldowanie', '$ulica_zameldowanie', '$kod_pocztowy_zameldowanie');";
  $mysqli->query($query);
  $id_zameldowanie = $mysqli->insert_id;
  $info["error_zameldowanie"] = $mysqli->error;
  $info["id_zameldowania"] = $id_zameldowanie;

  $query = "insert into zamieszkanie(miasto, ulica, kod_pocztowy) values('$miasto_zamieszkania', '$ulica_zamieszkania', '$kod_pocztowy_zamieszkania');";
  $mysqli->query($query);
  $id_zamieszkania = $mysqli->insert_id;
  $info["error_zamieszkanie"] = $mysqli->error;
  $info["id_zamieszkanie"] = $id_zamieszkania;

  $query = "insert into kontakty(telefon, telefon_stacionarny, email) values('$komorka', '$stacjonarny', '$email');";
  $mysqli->query($query);
  $id_kontakty = $mysqli->insert_id;
  $info["error_kontakt"] = $mysqli->error;
  $info["id_kontakt"] = $id_kontakty;

  $query = "insert into uczniowie(imie, drugie_imie, nazwisko, pesel, data_urodzenia, miejsce_urodzenia, id_zamieszkania, id_zameldowania, id_kontaktu, id_klasy, data_orzeczenie, samodzielne_poruszanie, archiwum) values('$imie', '$drugie_imie', '$nazwisko', '$pesel', '$data_urodzenia', '$miejsce_urodzenia', $id_zameldowanie, $id_zamieszkania, $id_kontakty, $id_klasy, '$data_orzeczenia', '$samodzielne_poruszanie', '$archiwum')";
  $mysqli->query($query);
  $info["error_uczen"] = $mysqli->error;
  $info["info"] = $mysqli->insert_id;
  $info["query"] = $query;
  $id_ucznia = $mysqli->insert_id;
  for($i = 0; $i < count($rodzic); $i++) {
    $miasto = $rodzic[$i]["zameldowanie"]["miasto"];
    $ulica = $rodzic[$i]["zameldowanie"]["ulica"];
    $kod = $rodzic[$i]["zameldowanie"]["kod"];
    $query = "insert into zameldowanie(miasto, ulica, kod_pocztowy) values('$miasto', '$ulica', '$kod');";
    $mysqli->query($query);
    $id_zameldowanie = $mysqli->insert_id;
    $info["error_zameldowanie"] = $mysqli->error;
    $info["id_zameldowania"] = $id_zameldowanie;

    $miasto = $rodzic[$i]["zamieszkania"]["miasto"];
    $ulica = $rodzic[$i]["zamieszkania"]["ulica"];
    $kod = $rodzic[$i]["zamieszkania"]["kod"];
    $query = "insert into zamieszkanie(miasto, ulica, kod_pocztowy) values('$miasto', '$ulica', '$kod');";
    $mysqli->query($query);
    $id_zamieszkania = $mysqli->insert_id;
    $info["error_zamieszkanie"] = $mysqli->error;
    $info["id_zamieszkanie"] = $id_zamieszkania;

    $komorka = $rodzic[$i]["telefon"];
    $stacjonarny = $rodzic[$i]["telefon_stacjonarny"];
    $email = $rodzic[$i]["email"];
    $query = "insert into kontakty(telefon, telefon_stacionarny, email) values('$komorka', '$stacjonarny', '$email');";
    $mysqli->query($query);
    $id_kontakty = $mysqli->insert_id;
    $info["error_kontakt"] = $mysqli->error;
    $info["id_kontakt"] = $id_kontakty;

    $query = "insert into rodzice(imie, drugie_imie, nazwisko, id_zamieszkania, id_zameldowania, id_kontaktu) values('".$rodzic[$i]["imie"]."', '".  $rodzic[$i]["drugie_imie"]."', '".$rodzic[$i]["nazwisko"]."', $id_zamieszkania, $id_zameldowanie, $id_kontakty)";
    $mysqli->query($query);
    $id_rodzica = $mysqli->insert_id;
    $info["error_rodzice"] = $mysqli->error;
    if(isset($id_rodzica))
      $mysqli->query("insert into uczniowie_rodzice(id_ucznia, id_rodzica) values($id_ucznia, $id_rodzica)");
    $info["error_relacja_rodzice"] = $mysqli->error;
    unset($id_rodzica);
  }
  $mysqli->close();
  echo json_encode($info);
?>
