-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 18 Kwi 2019, 10:59
-- Wersja serwera: 10.3.12-MariaDB
-- Wersja PHP: 7.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `szkola`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klasy`
--

CREATE TABLE `klasy` (
  `id` int(5) NOT NULL,
  `klasa` int(1) NOT NULL,
  `id_szkoly` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `klasy`
--

INSERT INTO `klasy` (`id`, `klasa`, `id_szkoly`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kontakty`
--

CREATE TABLE `kontakty` (
  `id` int(5) NOT NULL,
  `telefon` text COLLATE utf8_polish_ci NOT NULL,
  `telefon_stacionarny` text COLLATE utf8_polish_ci DEFAULT NULL,
  `email` text COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `nauczyciele`
--

CREATE TABLE `nauczyciele` (
  `id` int(5) NOT NULL,
  `imie` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `drugie_imie` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `nazwisko` varchar(90) COLLATE utf8_polish_ci NOT NULL,
  `pesel` varchar(120) COLLATE utf8_polish_ci NOT NULL,
  `rok_urodzenia` varchar(120) COLLATE utf8_polish_ci NOT NULL,
  `id_zamieszkania` int(11) NOT NULL,
  `id_zameldowania` int(11) NOT NULL,
  `id_kontaktu` int(11) NOT NULL,
  `archiwum` varchar(64) COLLATE utf8_polish_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przedmioty`
--

CREATE TABLE `przedmioty` (
  `id` int(3) NOT NULL,
  `skrot` varchar(64) COLLATE utf8_polish_ci DEFAULT NULL,
  `nazwa` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `przedmioty`
--

INSERT INTO `przedmioty` (`id`, `skrot`, `nazwa`) VALUES
(1, 'yDouUAT8SerYxTwdecJL9yV1MSOSOHISCT66YFb3N8Y=', 'yDouUAT8SerYxTwdecJL9zrTImkgpWmyZ2xZ9CC4R5QCFvAOcdE79993ueJKkZUnsAGUV3foq7ic0mTicI1EJw=='),
(6, 'Oba1bHRUhjMzLwAEfuumq6DHQhKMuZbBoitfUsA8mIU=\n', 'd755jriw01cbTwKgWt2rhszwfmf/q9Tfav4DRlEznj8=\n');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rodzice`
--

CREATE TABLE `rodzice` (
  `id` int(5) NOT NULL,
  `imie` text COLLATE utf8_polish_ci NOT NULL,
  `drugie_imie` text COLLATE utf8_polish_ci DEFAULT NULL,
  `nazwisko` text COLLATE utf8_polish_ci NOT NULL,
  `id_zamieszkania` int(11) NOT NULL,
  `id_zameldowania` int(11) NOT NULL,
  `id_kontaktu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `studia`
--

CREATE TABLE `studia` (
  `id` int(11) NOT NULL,
  `id_nauczyciela` int(11) NOT NULL,
  `uczelnia` text COLLATE utf8_polish_ci NOT NULL,
  `kierunek` text COLLATE utf8_polish_ci NOT NULL,
  `data_ukonczenia` text COLLATE utf8_polish_ci NOT NULL,
  `stopien` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `szkoly`
--

CREATE TABLE `szkoly` (
  `id` int(11) NOT NULL,
  `nazwa` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `szkoly`
--

INSERT INTO `szkoly` (`id`, `nazwa`) VALUES
(1, 'v0/l+0/3ZzaQa1zsc0z8Ni0aBCdvjiITc8ja/cG3V63Cbgk+6m5cOAJGpeRqxuUS\n');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczniowie`
--

CREATE TABLE `uczniowie` (
  `id` int(11) NOT NULL,
  `imie` text COLLATE utf8_polish_ci NOT NULL,
  `drugie_imie` text COLLATE utf8_polish_ci DEFAULT NULL,
  `nazwisko` text COLLATE utf8_polish_ci NOT NULL,
  `pesel` text COLLATE utf8_polish_ci NOT NULL,
  `data_urodzenia` text COLLATE utf8_polish_ci NOT NULL,
  `miejsce_urodzenia` text COLLATE utf8_polish_ci NOT NULL,
  `id_zamieszkania` text COLLATE utf8_polish_ci NOT NULL,
  `id_zameldowania` text COLLATE utf8_polish_ci NOT NULL,
  `id_kontaktu` text COLLATE utf8_polish_ci NOT NULL,
  `id_klasy` text COLLATE utf8_polish_ci NOT NULL,
  `data_orzeczenie` text COLLATE utf8_polish_ci NOT NULL,
  `samodzielne_poruszanie` text COLLATE utf8_polish_ci NOT NULL,
  `archiwum` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczniowie_rodzice`
--

CREATE TABLE `uczniowie_rodzice` (
  `id` int(11) NOT NULL,
  `id_ucznia` int(11) NOT NULL,
  `id_rodzica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczone_przedmioty`
--

CREATE TABLE `uczone_przedmioty` (
  `id` int(11) NOT NULL,
  `id_nauczyciela` int(11) NOT NULL,
  `id_przedmiotu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zameldowanie`
--

CREATE TABLE `zameldowanie` (
  `id` int(11) NOT NULL,
  `miasto` text COLLATE utf8_polish_ci NOT NULL,
  `ulica` text COLLATE utf8_polish_ci NOT NULL,
  `kod_pocztowy` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamieszkanie`
--

CREATE TABLE `zamieszkanie` (
  `id` int(11) NOT NULL,
  `miasto` text COLLATE utf8_polish_ci NOT NULL,
  `ulica` text COLLATE utf8_polish_ci NOT NULL,
  `kod_pocztowy` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `klasy`
--
ALTER TABLE `klasy`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `kontakty`
--
ALTER TABLE `kontakty`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `nauczyciele`
--
ALTER TABLE `nauczyciele`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pesel` (`pesel`);

--
-- Indeksy dla tabeli `przedmioty`
--
ALTER TABLE `przedmioty`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `rodzice`
--
ALTER TABLE `rodzice`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `studia`
--
ALTER TABLE `studia`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `szkoly`
--
ALTER TABLE `szkoly`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Nazwa` (`nazwa`(256));

--
-- Indeksy dla tabeli `uczniowie`
--
ALTER TABLE `uczniowie`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `uczniowie_rodzice`
--
ALTER TABLE `uczniowie_rodzice`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `uczone_przedmioty`
--
ALTER TABLE `uczone_przedmioty`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `zameldowanie`
--
ALTER TABLE `zameldowanie`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `zamieszkanie`
--
ALTER TABLE `zamieszkanie`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `klasy`
--
ALTER TABLE `klasy`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `kontakty`
--
ALTER TABLE `kontakty`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `nauczyciele`
--
ALTER TABLE `nauczyciele`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `przedmioty`
--
ALTER TABLE `przedmioty`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `rodzice`
--
ALTER TABLE `rodzice`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `studia`
--
ALTER TABLE `studia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `szkoly`
--
ALTER TABLE `szkoly`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `uczniowie`
--
ALTER TABLE `uczniowie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `uczniowie_rodzice`
--
ALTER TABLE `uczniowie_rodzice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `uczone_przedmioty`
--
ALTER TABLE `uczone_przedmioty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `zameldowanie`
--
ALTER TABLE `zameldowanie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `zamieszkanie`
--
ALTER TABLE `zamieszkanie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
