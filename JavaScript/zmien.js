function zmien_przedmiot_function(id) {
  console.log("zmien_przedmiot_function");
  json = {
    id: id,
    skrot: $("#" + id + "_skrot").val(),
    nazwa: $("#" + id + "_nazwa").val()
  }
  $.ajax({
    url: "Zmien/przedmiot.php",
    method: "post",
    data: json
  })
  .done(res => {
    $("#informacje").html("Zmieniono przedmiot");
    console.log(res);
  })
  .fail(error => {
    $("#informacje").html("Nie udało się zmienić przedmiotu");
    console.log(error);
  });
}

function zmien_szkola_klasy_function(id) {
  console.log("zmien_szkola_klasy_function");
  json = {
    id: id,
    nazwa: $("#" + id + "_nazwa").val(),
    ilosc: $("#" + id + "_liczba_klas").val()
  }
  $.ajax({
    url: "Usun/szkola_klasy.php",
    method: "post",
    data: json
  })
  .fail(error => {
    $("#informacje").html("Nie udało się zmienić Szkole");
    console.log(error);
  });

  $.ajax({
    url: "Zapisz/szkoly_klasy.php",
    method: "post",
    data: json
  })
  .fail(error => {
    $("#informacje").html("Nie udało się zmienić Szkole");
    console.log(error);
  });
}

function archiwizuj_uczen_function() {
  console.log("archiwizuj_uczen_function");
  $.ajax({
    url: "Zmien/uczen_archiwizuj.php",
    method: "post",
    data: {
      id: this.id
    }
  })
  .fail(error => {
    console.log(error);
  })
  .done(res => {
    $("#informacje").html("Nauczyciel został przeniesiony do archiwum")
    console.log(res);
  });
}

function archiwizuj_nauczyciel_function() {
  console.log("archiwizuj_nauczyciel_function");
  $.ajax({
    url: "Zmien/nauczyciel_archiwizuj.php",
    method: "post",
    data: {
      id: this.id
    }
  })
  .fail(error => {
    console.log(error);
  })
  .done(res => {
    $("#informacje").html("Nauczyciel został przeniesiony do archiwum")
    console.log(res);
  });
}

function edytuj_ucznia_function() {
  console.log("edytuj_ucznia_function");
  json = {
    imie: $("#imie_uczniowie_form").val(),
    drugie_imie: $("#drugie_imie_uczniowie_form").val(),
    nazwisko: $("#nazwisko_uczniowie_form").val(),
    data_urodzenia: $("#data_urodzenia_uczniowie_form").val(),
    miejsce_urodzenia: $("#miejsce_urodzenia_uczniowie_form").val(),
    data_orzeczenia: $("#data_orzeczenia_uczniowie_form").val(),
    samodzielne_poruszanie: $("#samodzielne_poruszanie_uczniowie_form").prop("checked"),
    miasto_zameldowanie: $("#miasto_zameldowania_uczniowie_form").val(),
    ulica_zameldowanie: $("#ulica_zameldowania_uczniowie_form").val(),
    kod_pocztowy_zameldowanie: $("#kod_pocztowy_zameldowania_uczniowie_form").val(),
    miasto_zamieszkania: $("#miasto_zamieszkania_uczniowie_form").val(),
    ulica_zamieszkania: $("#ulica_zamieszkania_uczniowie_form").val(),
    kod_pocztowy_zamieszkania: $("#kod_pocztowy_zamieszkania_uczniowie_form").val(),
    komorka: $("#telefon_uczniowie_form").val(),
    stacjonarny: $("#telefon_stacjonarny_uczniowie_form").val(),
    email: $("#email_uczniowie_form").val(),
    szkola: $("#szkola_uczniowie_form option:selected").text(),
    klasa: $("#klasa_uczniowie_form option:selected").text(),
    pesel: $("#pesel_uczniowie_form").val(),

    imie_rodzic1: $("#imie_rodzic1_form").val(),
    drugie_imie_rodzic1: $("#drugie_imie_rodzic1_form").val(),
    nazwisko_rodzic1: $("#nazwisko_rodzic1_form").val(),
    miasto_zamieszkania_rodzic1: $("#miasto_zamieszkania_rodzic1_form").val(),
    ulica_zamieszkania_rodzic1: $("#ulica_zamieszkania_rodzic1_form").val(),
    kod_pocztowy_zamieszkania_rodzic1: $("#kod_pocztowy_zamieszkania_rodzic1_form").val(),
    miasto_zameldowanie_rodzic1: $("#miasto_zameldowania_rodzic1_form").val(),
    ulica_zameldowanie_rodzic1: $("#ulica_zameldowania_rodzic1_form").val(),
    kod_pocztowy_zameldowanie_rodzic1: $("#kod_pocztowy_zameldowania_rodzic1_form").val(),
    telefon_rodzic1: $("#telefon_rodzic1_form").val(),
    telefon_stacjonarny_rodzic1: $("#telefon_stacjonarny_rodzic1_form").val(),
    email_rodzic1: $("#email_rodzic1_form").val(),

    imie_rodzic2: $("#imie_rodzic2_form").val(),
    drugie_imie_rodzic2: $("#drugie_imie_rodzic2_form").val(),
    nazwisko_rodzic2: $("#nazwisko_rodzic2_form").val(),
    miasto_zamieszkania_rodzic2: $("#miasto_zamieszkania_rodzic2_form").val(),
    ulica_zamieszkania_rodzic2: $("#ulica_zamieszkania_rodzic2_form").val(),
    kod_pocztowy_zamieszkania_rodzic2: $("#kod_pocztowy_zamieszkania_rodzic2_form").val(),
    miasto_zameldowania_rodzic2: $("#miasto_zameldowania_rodzic2_form").val(),
    ulica_zameldowania_rodzic2: $("#ulica_zameldowania_rodzic2_form").val(),
    kod_pocztowy_zameldowania_rodzic2: $("#kod_pocztowy_zameldowania_rodzic2_form").val(),
    telefon_rodzic2: $("#telefon_rodzic2_form").val(),
    telefon_stacjonarny_rodzic2: $("#telefon_stacjonarny_rodzic2_form").val(),
    email_rodzic2: $("#email_rodzic2_form").val()
  }

  for(i = 0; i < uczniowie.length; i++)
    if(uczniowie[i]["id"] == $(this).attr("data-id"))
      stare = uczniowie[i];


  $.ajax({
    url: "Zmien/uczen_rodzice.php",
    method: "post",
    data: {
      nowe: json,
      stare: stare
    }
  })
  .done(res => {
    console.log(res);
    $("#informacje").html("Dane zostały zmienione");
  })
  .fail(error => {
    console.log(error);
  })
}

function edytuj_nauczyciela_function() {
  console.log("dodaj_nauczyciela_function");
  var dane_studia = Array();
  for(i = 0; i < $(".studia_uczelnia_nauczyciele_form").length; i++) {
    dane_studia[i] = {};
    dane_studia[i]["uczelnia"] = $(".studia_uczelnia_nauczyciele_form").eq(i).val();
    dane_studia[i]["kierunek"] = $(".studia_kierunek_nauczyciele_form").eq(i).val();
    dane_studia[i]["data"] = $(".studia_data_ukonczenia_nauczyciele_form").eq(i).val();
    dane_studia[i]["stopien"] = $(".studia_stopien_nauczyciele_form :checked").eq(i).text();
  }
  json = {
    imie: $("#imie_nauczyciele_form").val(),
    drugie_imie: $("#drugie_imie_nauczyciele_form").val(),
    nazwisko: $("#nazwisko_nauczyciele_form").val(),
    pesel: $("#pesel_nauczyciele_form").val(),
    rok_urodzenia: $("#rok_urodzenia_nauczyciele_form").val(),
    zamieszkanie: {
      miasto: $("#miasto_zamieszkania_nauczyciele_form").val(),
      ulica: $("#ulica_zamieszkania_nauczyciele_form").val(),
      kod: $("#kod_pocztowy_zamieszkania_nauczyciele_form").val(),
    },
    zameldowanie: {
      miasto: $("#miasto_zameldowania_nauczyciele_form").val(),
      ulica: $("#ulica_zameldowania_nauczyciele_form").val(),
      kod: $("#kod_pocztowy_zameldowania_nauczyciele_form").val(),
    },
    kontakt: {
      telefon: $("#telefon_nauczyciele_form").val(),
      stacjonarny: $("#telefon_stacjonarny_nauczyciele_form").val(),
      email: $("#email_nauczyciele_form").val()
    },
    przedmioty: $("#uczone_przedmioty_nauczyciele_form").val(),
    studia: dane_studia
  }

  for(i = 0; i < nauczyciele.length; i++)
    if(nauczyciele[i]["id"] == $(this).attr("data-id"))
      stare = nauczyciele[i];
  console.log(stare);

  $.ajax({
    url: "Zmien/nauczyciel.php",
    method: "post",
    data: {
      nowe: json,
      stare: stare
    }
  })
  .done(res => {
    console.log(res);
  })
  .fail(error => {
    console.log(error);
  });
}
