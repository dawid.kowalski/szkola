<?php
  if($_SERVER['REQUEST_METHOD'] != 'POST')
    exit();
  require '../connect.php';
  require '../szyfruj.php';
  $mysqli = new mysqli($host, $user, $password, $database);
  $query = "select klasy.id_szkoly as id, szkoly.nazwa as nazwa, COUNT(klasy.id) as liczba_klas from klasy, szkoly where szkoly.id = klasy.id_szkoly GROUP by klasy.id_szkoly";
  $result = $mysqli->query($query);
  while($value = $result->fetch_assoc()) {
    $szkola["id"] = $value["id"];
    $szkola["nazwa"] = decrypt($value["nazwa"]);
    $szkola["nazwa"] = str_replace("\0", '' ,$szkola["nazwa"]);

    $szkola["liczba_klas"] = $value["liczba_klas"];
    $szkoly[] = $szkola;
  }
  if(isSet($szkoly))
    echo json_encode($szkoly);
  else {
    $szkoly = null;
    echo json_encode($szkoly);
  }
?>
