<?php
function CheckPESEL($pesel)
{
	if (!preg_match('/^[0-9]{11}$/',$pesel)) //sprawdzamy czy ciąg ma 11 cyfr
	{
		return false;
	}

	$wagi = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3); // tablica z odpowiednimi wagami
	$suma = 0;
	for ($i = 0; $i < 10; $i++)
	{
		$suma += $wagi[$i] * $pesel[$i]; //mnożymy każdy ze znaków przez wagć i sumujemy wszystko
	}
	$kontrolny = 10 - $suma % 10;
	if ($kontrolny == $pesel[10]) return true;
	return false;
}
?>
