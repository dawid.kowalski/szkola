<?php
  if($_SERVER['REQUEST_METHOD'] != 'POST')
    exit();
  require '../connect.php';
  require '../szyfruj.php';
  $mysqli = new mysqli($host, $user, $password, $database);
  $query = "select * from przedmioty order by nazwa";
  $result = $mysqli->query($query);
  while($value = $result->fetch_assoc()) {
    $przedmiot["id"] = $value["id"];
    $przedmiot["nazwa"] = decrypt($value["nazwa"]);
    $przedmiot["skrot"] = decrypt($value["skrot"]);
    $przedmiot["nazwa"] = str_replace("\0", '', $przedmiot["nazwa"]);
    $przedmiot["skrot"] = str_replace("\0", '', $przedmiot["skrot"]);
    $przedmioty[] = $przedmiot;
  }

  if(isSet($przedmioty))
    echo json_encode($przedmioty);
  else {
    $przedmioty = null;
    echo json_encode($przedmioty);
  }
?>
