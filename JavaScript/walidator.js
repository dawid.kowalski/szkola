function walidator_form_function(formularz){
  console.log("walidator_function");
  if(formularz === 'uczen')
    var form = $("#uczniowie_form :input");
  else
    var form = $("#nauczyciele_form :input");

  var blad = false;
  form.each(function() {
    if(($(this).attr("required") == "required") && ($(this).val() == "") && (!$(this).is("button"))) {
      $(this).css("border-color", "red");
      console.log(this);
      blad = true;
    }
    else if(($(this).is("select")) && (this.options.selectedIndex == 0)) {
      $(this).css("border-color", "red");
      console.log(this);
      blad = true;
    }
    else if(($(this).attr("data-type") == "pesel") && ($(this).val().length != 11)) {
      $(this).css("border-color", "red");
      console.log("pesel");
      blad = true;
    }
  });
  if(blad == true) {
    return false;
    $("#informacje").html("Nie wszystkie pola są uzupełnione");
  }
  else
    return true;
}
