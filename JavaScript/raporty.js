function raport_formularz_function() {
  console.log("raport_formularz_function");
  raport_formularz_pola = new Array();
  switch ($("#kto_raport option:selected").text()) {
    case 'Nauczyciele':
      raport_formularz_pola = ["imie", "drugie_imie", "nazwisko", "pesel", "rok_urodzenia", "miejsce_zamieszkania", "miejsce_zameldowania", "telefon_komorkowy", "telefon_stacionarny", "email", "przedmioty", "studia", "archiwum"];

      $("#pola_formularz_raporty").html(`
          <h2>Jakie dane</h2>
          <div class = 'wiersz_formularz_raporty'>
            <div class = 'opcja_formularz_raport opcja_formularz_raport_nazwa'>Nazwa</div>
            <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'>Widoczne</div>
            <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'>Sortuj</div>
            <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'>Wyżej</div>
            <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'>Niżej</div>
          </div>
        `);
        for(nr in raport_formularz_pola) {
          nazwa = raport_formularz_pola[nr].charAt(0).toUpperCase() + raport_formularz_pola[nr].slice(1);
          nazwa = nazwa.replace("_", " ");
          $("#pola_formularz_raporty").append(`
            <div class = 'wiersz_formularz_raporty'>
              <div class = 'opcja_formularz_raport opcja_formularz_raport_nazwa'>${nazwa}</div>
              <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'><input class = 'checkbox_formularz_raport' id = "widoczne_${raport_formularz_pola[nr]}_formularz_raport" type="checkbox"></div>
              <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'><input class = 'checkbox_formularz_raport' id = "sortuj_${raport_formularz_pola[nr]}_formularz_raport" type="radio" name = "sortuj_formularz_raport"></div>
              <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'><button class = 'button_formularz_raport' id = "lewo_${raport_formularz_pola[nr]}_formularz_raport">&uarr;</button></div>
              <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'><button class = 'button_formularz_raport' id = "prawo_${raport_formularz_pola[nr]}_formularz_raport">&darr;</button></div>
            </div>
          `);
        }
      $("#filtrowanie_formularz_raporty").html(`
          <h2>Filtrowanie</h2>
          <div><label>Archiwum: <select id = 'archiwum_filtr_raporty'> <option value = '-1'>BRAK</option> <option value = '1'>TAK</option> <option value = '0'>NIE</option></select></label></div>
          <div><label>Przedmioty: <select id = 'przedmiot_filtr_raporty' multiple></select> </label></div>
        `);
        let przedmioty = pobierz_przedmioty_lista_function();
        for (i = 0; i < przedmioty.length; i++) {
          $("#przedmiot_filtr_raporty").append(`<option value = '${przedmioty[i]["id"]}'>${przedmioty[i]["nazwa"]}</option>`);
        }
        $("#przedmiot_filtr_raporty").css("height", `${przedmioty.length *2.5}rem`);
      break;
    case 'Uczniowie':
      raport_formularz_pola = ["imie", "drugie_imie", "nazwisko",  "pesel", "data_urodzenia", "miejsce_urodzenia", "miejsce_zamieszkania", "miejsce_zameldowania", "telefon_komorkowy", "telefon_stacionarny", "email", "szkola" , "klasa" , "data_orzeczenia", "samodzielne_poruszanie","archiwum"];

      $("#pola_formularz_raporty").html(`
          <h2>Jakie dane</h2>
          <div class = 'wiersz_formularz_raporty'>
            <div class = 'opcja_formularz_raport opcja_formularz_raport_nazwa'>Nazwa</div>
            <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'>Widoczne</div>
            <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'>Sortuj</div>
            <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'>Wyżej</div>
            <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'>Niżej</div>
          </div>
        `);
        for(nr in raport_formularz_pola) {
          nazwa = raport_formularz_pola[nr].charAt(0).toUpperCase() + raport_formularz_pola[nr].slice(1);
          nazwa = nazwa.replace("_", " ");
          $("#pola_formularz_raporty").append(`
            <div class = 'wiersz_formularz_raporty'>
              <div class = 'opcja_formularz_raport opcja_formularz_raport_nazwa'>${nazwa}</div>
              <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'><input class = 'checkbox_formularz_raport' id = "widoczne_${raport_formularz_pola[nr]}_formularz_raport" type="checkbox"></div>
              <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'><input class = 'checkbox_formularz_raport' id = "sortuj_${raport_formularz_pola[nr]}_formularz_raport" type="radio" name = "sortuj_formularz_raport"></div>
              <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'><button class = 'button_formularz_raport' id = "lewo_${raport_formularz_pola[nr]}_formularz_raport">&uarr;</button></div>
              <div class = 'opcja_formularz_raport opcja_formularz_raport_edycja'><button class = 'button_formularz_raport' id = "prawo_${raport_formularz_pola[nr]}_formularz_raport">&darr;</button></div>
            </div>
          `);
        }
      $("#filtrowanie_formularz_raporty").html(`
          <h2>Filtrowanie</h2>
          <div><label>Archiwum: <select id = 'archiwum_filtr_raporty'> <option value = '-1'>BRAK</option> <option value = '1'>TAK</option> <option value = '0'>NIE</option></select></label></div>
          <div><label>Szkoły/klasy: <select id = 'szkoly_filtr_raporty' multiple></select> </label></div>
        `);
        szkoly_klasy = pobierz_szkoly_lista_function();
        if(szkoly_klasy == null) $("#informacje").html("Jeżeli chcesz wybrac uczniów ze szkoły/klasy dodaj je");
        console.log(szkoly_klasy);
      break;
  }
}

function pobierz_dane_raport_function() {
  console.log("pobierz_dane_raport_function");
  switch ($("#kto_raport option:selected").text()) {
    case 'Nauczyciele':
      let raport = {};
      raport["widoczne"] = {};
      raport["przedmioty"] = {};

      let raport_formularz_pola = ["imie", "drugie_imie", "nazwisko", "pesel", "rok_urodzenia", "miejsce_zamieszkania", "miejsce_zameldowania", "telefon_komorkowy", "telefon_stacionarny", "email", "przedmioty", "studia", "archiwum"];
      for(nr in raport_formularz_pola) {
        if ($(`#widoczne_${raport_formularz_pola[nr]}_formularz_raport`).prop('checked') == true) raport["widoczne"][nr] = raport_formularz_pola[nr];
        if ($(`#sortuj_${raport_formularz_pola[nr]}_formularz_raport`).prop('checked') == true) raport["sortuj"] = raport_formularz_pola[nr];
      }
      i = 0;
      $("#przedmiot_filtr_raporty option:selected").each(function() {
        raport["przedmioty"][i] = $(this).val();
        i++;
      });

      console.log(raport);
      $.ajax({
        method: "POST",
        async: "false",
        url: "Raport/Nauczyciele.php",
        data: {
          widoczne: raport["widoczne"],
          sortuj: raport["sortuj"],
          archiwum: $("#archiwum_filtr_raporty option:selected").text(),
          przedmioty: raport["przedmioty"]
        }
      })
      .done(res => {
        console.log(res);
        podglad_raport_function( JSON.parse(res) );
      })
      .fail(error => {
        console.error(error);
      });
      break;
  }
}

function podglad_raport_function(dane) {
  console.log("podglad_raport_function");
  if(dane != null){
    html = "<table><tr>";
    for (var i in dane[0]) {
      i = i.replace("_", " ");
      i = i.charAt(0).toUpperCase() + i.slice(1);
      html += `<th>${i}</th>`;
    }
    html += "</tr>";
    for (var wiersz = 0; wiersz < dane.length; wiersz++) {
      html += "<tr>";
      for(j in dane[wiersz]) {
        html += `<td>${dane[wiersz][j]}</td>`;
      }
      html += "</tr>";
    }
    html += "</table>";
    $("#raport_podglad").html(html);
  }
  else $("#raport_podglad").html('<h1>Nie ma danych spełniających wszystkie kryteria</h1>');
}

function zapisz_raport_function() {
  console.log("zapisz_raport_function");
  html = $("#raport_podglad").html();
  $.ajax({
    url: "Raport/Generator/index.php",
    method: "POST",
    data: {
      tabela: html
    }
  })
  .fail(error => {
    console.error(error);
  })
  .done(res => {
    console.log(res);
    window.open(res);
  });
}
