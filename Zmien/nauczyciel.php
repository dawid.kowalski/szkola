<?php
  require '../connect.php';
  require '../szyfruj.php';

  $mysqli = new mysqli($host, $user, $password, $database);
  $stare = $_POST["stare"];
  $nowe = $_POST["nowe"];
  foreach($nowe as &$value) {
    if(is_string($value)) {
      $value = htmlspecialchars($value);
      $value = $mysqli->real_escape_string($value);
      $value = encrypt($value);
    }
  }
  for ($i=0; $i < count($nowe["studia"]); $i++) {
    foreach($nowe["studia"][$i] as &$value) {
      $value = htmlspecialchars($value);
      $value = $mysqli->real_escape_string($value);
      $value = encrypt($value);
    }
  }

  foreach($nowe["zameldowanie"] as $key => &$value) {
    $value = htmlspecialchars($value);
    $value = $mysqli->real_escape_string($value);
    $value = encrypt($value);
  }

  foreach($nowe["zamieszkanie"] as $key => &$value) {
    $value = htmlspecialchars($value);
    $value = $mysqli->real_escape_string($value);
    $value = encrypt($value);
  }

  foreach($nowe["kontakt"] as $key => &$value) {
    $value = htmlspecialchars($value);
    $value = $mysqli->real_escape_string($value);
    $value = encrypt($value);
  }

  $query = "update nauczyciele set imie = '".$nowe["imie"]."', drugie_imie = '".$nowe["drugie_imie"]."', nazwisko = '".$nowe["nazwisko"]."', pesel = '".$nowe["pesel"]."', rok_urodzenia = '".$nowe["rok_urodzenia"]."', archiwum = '".$stare["archiwum"]."' where id = ".$stare["id"];
  $mysqli->query($query);
  // echo $mysqli->error;
  $query = "update zameldowanie set miasto = '".$nowe["zameldowanie"]["miasto"]."', ulica = '".$nowe["zameldowanie"]["ulica"]."', kod_pocztowy ='".$nowe["zameldowanie"]["kod"]."' where id = ".$stare["id_zameldowania"];
  $mysqli->query($query);
  $query = "update zamieszkanie set miasto = '".$nowe["zamieszkanie"]["miasto"]."', ulica = '".$nowe["zamieszkanie"]["ulica"]."', kod_pocztowy ='".$nowe["zamieszkanie"]["kod"]."' where id = ".$stare["id_zamieszkania"];
  $mysqli->query($query);

  $query = "update kontakty set telefon = '".$nowe["kontakt"]["telefon"]."', telefon_stacionarny = '".$nowe["kontakt"]["stacjonarny"]."', email ='".$nowe["kontakt"]["email"]."' where id = ".$stare["id_kontaktu"].";";
  $mysqli->query($query);
  echo $mysqli->error;

  for ($i = 0; $i < count($nowe["studia"]); $i++) {
    $studia = $nowe["studia"][$i];
    $studia["uczelnia"] = htmlspecialchars($studia["uczelnia"]);
    $studia["uczelnia"] = $mysqli->real_escape_string($studia["uczelnia"]);
    $studia["uczelnia"] = encrypt($studia["uczelnia"]);

    $studia["kierunek"] = htmlspecialchars($studia["kierunek"]);
    $studia["kierunek"] = $mysqli->real_escape_string($studia["kierunek"]);
    $studia["kierunek"] = encrypt($studia["kierunek"]);

    $studia["data"] = htmlspecialchars($studia["data"]);
    $studia["data"] = $mysqli->real_escape_string($studia["data"]);
    $studia["data"] = encrypt($studia["data"]);

    $studia["stopien"] = htmlspecialchars($studia["stopien"]);
    $studia["stopien"] = $mysqli->real_escape_string($studia["stopien"]);
    $studia["stopien"] = encrypt($studia["stopien"]);

    $query = "update studia set uczelnia = '".$studia["uczelnia"]."', kierunek = '".$studia["kierunek"]."', data_ukonczenia = '".$studia["data"]."', stopien = '".$studia["stopien"]."' where id = ".$stare["studia"][$i]["id"];
    $mysqli->query($query);
    echo $mysqli->error;
  }
?>
