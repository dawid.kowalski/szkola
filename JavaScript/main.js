var uczniowie, nauczyciele;
$(document).ready(function() {
  console.log("jQuery załadowane");
  $("#Nauczyciele").on("click", Nauczyciele_function);
  $(document).on("change", "#sortowanie_nauczyciele_lista" ,pobierz_nauczyciele_lista_function);
  $(document).on("change", "#archiwum_nauczyciele_lista" ,pobierz_nauczyciele_lista_function);
  $("#Przedmioty").on("click", Przedmioty_function);
  $("#Szkoly").on("click", Szkoly_function);
  $("#Uczniowie").on("click", Uczniowie_function);
  $("#Raporty").on("click", Raporty_function);
  $(document).on("click", "button", function(event) {
    // Wyłącza domyślne zdarzenia formularza html
    event.preventDefault();
  });
  $(document).on("click", ".uczen_button", function() {
    id = $(this).attr("id");
    $(`#${id}_wyswietl`).html($("#" + id + "_" + $(this).attr("data-operacja") + "_uczen_div").html());
    if(($(`#${id}_wyswietl`).is(":visible")) && ($(this).attr("data-operacja") == $(`#${id}_wyswietl`).attr("data-wyswietlane"))) {
      $(`#${id}_wyswietl`).slideUp();
    }
    else {
      id = $(this).attr("id");
      typ = $(this).attr("data-operacja");
      if(typeof typ !== 'undefined') {
        wyswietl_dane_uczen_function(id, typ);
        $(`#${id}_wyswietl`).slideDown();
        $(`#${id}_wyswietl`).attr("data-wyswietlane", typ);
      }
    }
  });
  $(document).on("click", ".nauczyciel_button", function() {
    id = $(this).attr("id");
    $(`#${id}_wyswietl`).html($("#" + id + "_" + $(this).attr("data-operacja") + "_nauczyciel_div").html());
    if(($(`#${id}_wyswietl`).is(":visible")) && ($(this).attr("data-operacja") == $(`#${id}_wyswietl`).attr("data-wyswietlane"))) {
      $(`#${id}_wyswietl`).slideUp();
    }
    else {
      id = $(this).attr("id");
      typ = $(this).attr("data-operacja");
      if(typeof typ !== 'undefined') {
        wyswietl_dane_nauczyciel_function(id, typ);
        $(`#${id}_wyswietl`).slideDown();
        $(`#${id}_wyswietl`).attr("data-wyswietlane", typ);
      }
    }
  });
  $(document).on("click", ".uczen_button_edytuj", edytuj_uczen_form_function);
  $(document).on("click", ".nauczyciel_button_edytuj", edytuj_nauczyciel_form_function);
  $(document).on("keyup", "#szukaj_uczniowie_lista", pobierz_uczniowie_lista_function);
  $(document).on("keyup", "#szukaj_nauczyciele_lista", pobierz_nauczyciele_lista_function);
  $(document).on("click", ".uczen_button_archiwizuj", archiwizuj_uczen_function);
  $(document).on("click", ".nauczyciel_button_archiwizuj", archiwizuj_nauczyciel_function);
  $(document).on("change", "#sortowanie_uczniowie_lista", pobierz_uczniowie_lista_function);
  $(document).on("change", "#archiwum_uczniowie_lista", pobierz_uczniowie_lista_function);
  $(document).on("click", "#zapisz_uczniowie_form", dodaj_ucznia_function);
  $(document).on("click", "#edytuj_uczniowie_button", edytuj_ucznia_function);
  $(document).on("click", "#edytuj_nauczyciela_button", edytuj_nauczyciela_function);
  $(document).on("click", "#wyswietl_uczniowie_form", uczniowie_form_function);
  $(document).on("click", "#dodaj_przedmiot_form", function() {dodaj_przedmiot_function(); Przedmioty_function();});
  $(document).on("keyup", "input", maska_wprowadzania_function);
  $(document).on("click", "#dodaj_nauczyciela_form", nauczyciel_form_function);
  $(document).on("click", "#dodaj_nauczyciela", dodaj_nauczyciela_function);
  $(document).on("mousedown", "#uczone_przedmioty_nauczyciele_form option", function(event) {
    event.preventDefault();
    $(this).prop('selected', !$(this).prop('selected'));
  });
  $(document).on("mousedown", "#przedmiot_filtr_raporty option", function(event) {
    event.preventDefault();
    $(this).prop('selected', !$(this).prop('selected'));
  });
  $(document).on("click", "#dodaj_studia_nauczyciele_form", function() {
    var html = `<div class = 'form_element'><label>Nazwa uczelni *: <input required type = 'text' class = 'studia_uczelnia_nauczyciele_form'></label></div>
          <div class = 'form_element'><label> *: <input type = 'text' class = 'studia_kierunek_nauczyciele_form'></label></div>
          <div class = 'form_element'><label>Data ukończenia *: <input required type = 'date' class = 'studia_data_ukonczenia_nauczyciele_form'></label></div>
          <div class = 'form_element'><label>Stopień *: <select class = 'studia_stopien_nauczyciele_form'><option>Wybierz</option><option>I</option><option>II</option><option>III</option></select></label></div>`;

    $("#studia_nauczyciele_form").prepend(html);
  });
  $(document).on("change", "#kto_raport", raport_formularz_function);
  $(document).on("change", ".checkbox_formularz_raport", pobierz_dane_raport_function);
  $(document).on("click", "#przedmiot_filtr_raporty option", pobierz_dane_raport_function);
  $(document).on("change", "#archiwum_filtr_raporty", pobierz_dane_raport_function);
  $(document).on("click", "#zapisz_raport", zapisz_raport_function);
});

function Nauczyciele_function() {
  console.log("Nauczyciele_function");
  $("#informacje").empty();
  $("#zawartosc").empty();
  html = `<div class = "menu_zawartosc">
  <div><button id = "dodaj_nauczyciela_form">Dodaj Nauczyciela</button></div>
  <div><label>Szukaj <input title = 'Wpisz informację taką jak imie, nazawisko, miasto, numer telefonu' type = 'text' id = 'szukaj_nauczyciele_lista'></label></div>
  <div><label>Kto: <select id = 'archiwum_nauczyciele_lista'>
    <option value = 'wszyscy'>Wszyscy</option>
    <option value = 'archiwum'>Archiwum</option>
    <option value = 'nauczyciel'>Aktualni</option>
  </select></label></div>

  <div><label>Sortowanie: <select id = "sortowanie_nauczyciele_lista">
    <option value = 'nazw a->z'>[Nazwiska] Od A do Z </option>
    <option value = 'nazw z->a'>[Nazwiska] Od Z do A </option>
    <option value = 'prze a->z'>[Przedmioty] Od A do Z </option>
    <option value = 'prze z->a'>[Przedmioty] Od Z do A </option>
  </select></label></div>
  </div>
  <div id = "lista_nauczyciele_div"></div>
  `;
  $("#zawartosc").html(html);
  pobierz_nauczyciele_lista_function();
}

function nauczyciel_form_function(edytuj_array) {
  console.log("dodaj_nauczyciela_form_function");
  if(typeof edytuj_array["id"] === 'undefined') {
    edytuj_array["zamieszkanie"] = {"miasto": "", "ulica": "", "kod": ""};
    edytuj_array["zameldowanie"] = {"miasto": "", "ulica": "", "kod": ""};
    edytuj_array["studia"] = {"uczelnia": "", "kierunek": "", "data": ""};
    edytuj_array["kontakt"] = {"telefon": "", "telefon_stacjonarny": "", "email": ""};
  }


  $("#lista_nauczyciele_div").empty();
  html = `<h3>Dodaj nauczyciela</h3>
  <form id = 'nauczyciele_form' action = ''><div class = 'formularz'>
    <fieldset><legend>Dane nauczyciela</legend>
      <div class = 'form_element'><label>Imie *: <input value = '${edytuj_array["imie"] || ""}' data-type = 'imie/nazwisko' type = 'text' id = 'imie_nauczyciele_form' autofocus required></label></div>
      <div class = 'form_element'><label>Drugie imie: <input value = '${edytuj_array["drugie_imie"] || ""}' data-type = 'imie/nazwisko' type = 'text' id = 'drugie_imie_nauczyciele_form'></label></div>
      <div class = 'form_element'><label>Nazwisko *: <input value = '${edytuj_array["nazwisko"] || ""}' data-type = 'imie/nazwisko' type = 'text' id = 'nazwisko_nauczyciele_form' required></label></div>
      <div class = 'form_element'><label>Pesel *: <input value = '${edytuj_array["pesel"] || ""}' data-type = 'pesel' type = 'text' id = 'pesel_nauczyciele_form' required></label></div>
      <div class = 'form_element'><label>Rok urodzenia *: <input value = '${edytuj_array["rok_urodzenia"] || ""}' data-type = 'rok_urodzenia' type = 'number' id = 'rok_urodzenia_nauczyciele_form' required></label></div>
    </fieldset>

    <fieldset><legend>Zamieszkanie</legend>
      <div class = 'form_element'><label>Miasto *: <input value = '${edytuj_array["zamieszkanie"]["miasto"] || ""}' data-type = 'miejsce' required type = 'text' id = 'miasto_zamieszkania_nauczyciele_form'></label></div>
      <div class = 'form_element'><label>Ulica *: <input value = '${edytuj_array["zamieszkanie"]["ulica"] || ""}' data-type = 'ulica' required type = 'text' id = 'ulica_zamieszkania_nauczyciele_form'></label></div>
      <div class = 'form_element'><label>Kod pocztowy *: <input value = '${edytuj_array["zamieszkanie"]["kod"] || ""}' data-type = 'kod_pocztowy' required type = 'text' id = 'kod_pocztowy_zamieszkania_nauczyciele_form' placeholder = 'xx-xxx'></label></div>
    </fieldset>

    <fieldset><legend>Zameldowanie</legend>
      <div class = 'form_element'><label>Miasto *: <input value = '${edytuj_array["zameldowanie"]["miasto"] || ""}' data-type = 'miejsce' required type = 'text' id = 'miasto_zameldowania_nauczyciele_form'></label></div>
      <div class = 'form_element'><label>Ulica *: <input value = '${edytuj_array["zameldowanie"]["ulica"] || ""}' data-type = 'ulica' required type = 'text' id = 'ulica_zameldowania_nauczyciele_form'></label></div>
      <div class = 'form_element'><label>Kod pocztowy *: <input value = '${edytuj_array["zameldowanie"]["kod"] || ""}' data-type = 'kod_pocztowy' required type = 'text' id = 'kod_pocztowy_zameldowania_nauczyciele_form' placeholder = 'xx-xxx' ></label></div>
    </fieldset>

    <fieldset><legend>Kontakt</legend>
      <div class = 'form_element'><label>Telefon *: <input value = '${edytuj_array["kontakt"]["telefon"]|| ""}' placeholder = '123-456-789'  data-type = 'komorka' required type = 'tel' id = 'telefon_nauczyciele_form'></label></div>
      <div class = 'form_element'><label>Telefon Stacjonarny: <input value = '${edytuj_array["kontakt"]["telefon_stacjonarny"] || ""}' placeholder = '123-45-79'  data-type = 'stacjonarny'  type = 'tel' id = 'telefon_stacjonarny_nauczyciele_form'></label></div>
      <div class = 'form_element'><label>Email *: <input required value = '${edytuj_array["kontakt"]["email"] || ""}' type = 'email' id = 'email_nauczyciele_form'></label></div>
    </fieldset>

    <fieldset id = "studia_nauczyciele_form"><legend>Studia</legend>
    </fieldset>

    <div class = 'form_element'><label>Wybierz uczone przedmioty *:<select id = 'uczone_przedmioty_nauczyciele_form' multiple><option>Wybierz</option></select></label></div>
    <div class = 'form_element'><button data-id = '${edytuj_array["id"]}' id = 'dodaj_nauczyciela'>Zapisz</button></div>
  </form>`;
  $("#lista_nauczyciele_div").html(html);
  przedmioty = pobierz_przedmioty_lista_function();
  przedmioty.forEach(przedmiot => {
    $("#uczone_przedmioty_nauczyciele_form").append(`<option value = "${przedmiot["id"]}">${przedmiot["nazwa"]}</option>`);
  });
  if(typeof edytuj_array["id"] !== 'undefined') {
    edytuj_array["studia"].forEach((studia) => {
      let element = `<div style = 'border: dotted .1rem white; border-radius: 2rem; margin: 2rem 0rem;'>
            <div class = 'form_element'><label>Nazwa uczelni *: <input value = '${edytuj_array["studia"][0]["uczelnia"]|| ""}' required type = 'text' class = 'studia_uczelnia_nauczyciele_form'></label></div>
            <div class = 'form_element'><label>Kierunek *: <input value = '${edytuj_array["studia"][0]["kierunek"] || ""}'  type = 'text' class = 'studia_kierunek_nauczyciele_form' required></label></div>
            <div class = 'form_element'><label>Data ukończenia *: <input required value = '${edytuj_array["studia"][0]["data_ukonczenia"] || ""}' type = 'date' class = 'studia_data_ukonczenia_nauczyciele_form'></label></div>`;
            if(edytuj_array["studia"][0]["stopien"] == 'I') element += `<div class = 'form_element'><label>Stopień *: <select class = 'studia_stopien_nauczyciele_form'><option>Wybierz</option><option selected>I</option><option>II</option><option>III</option></select></label></div>`;
            else if(edytuj_array["studia"][0]["stopien"] == 'II') element += `<div class = 'form_element'><label>Stopień *: <select class = 'studia_stopien_nauczyciele_form'><option>Wybierz</option><option>I</option><option selected>II</option><option>III</option></select></label></div>`;
            else if(edytuj_array["studia"][0]["stopien"] == 'III') element += `<div class = 'form_element'><label>Stopień *: <select class = 'studia_stopien_nauczyciele_form'><option>Wybierz</option><option>I</option><option>II</option><option selected>III</option></select></label></div>`;
            element += `<div class = 'form_element'><button id = 'dodaj_studia_nauczyciele_form'>Dodaj</button></div>
          </div>`;
      $("#studia_nauczyciele_form").append(element);
    });

    $("#uczone_przedmioty_nauczyciele_form option").each( function() {
      for (var i = 0; i < edytuj_array["przedmioty"].length ;i++) {
        if(edytuj_array["przedmioty"][i] == $(this).text()) {
          console.log($(this).text());
          $(this).prop('selected', true);
        }
      }
    });
    $("#dodaj_nauczyciela").attr("id", "edytuj_nauczyciela_button");
    $("#edytuj_nauczyciela_button").html("Edytuj");
    }
    else {
      let element = `<div style = 'border: dotted .1rem white; border-radius: 2rem; margin: 2rem 0rem;'>
            <div class = 'form_element'><label>Nazwa uczelni *: <input required type = 'text' class = 'studia_uczelnia_nauczyciele_form'></label></div>
            <div class = 'form_element'><label>Kierunek *: <input type = 'text' class = 'studia_kierunek_nauczyciele_form' required></label></div>
            <div class = 'form_element'><label>Data ukończenia *: <input required type = 'date' class = 'studia_data_ukonczenia_nauczyciele_form'></label></div>
            <div class = 'form_element'><label>Stopień *: <select class = 'studia_stopien_nauczyciele_form'><option>Wybierz</option><option>I</option><option>II</option><option>III</option></select></label></div>
            <div class = 'form_element'><button id = 'dodaj_studia_nauczyciele_form'>Dodaj</button></div>
          </div>`;
      $("#studia_nauczyciele_form").append(element);
    }

  $("#uczone_przedmioty_nauczyciele_form").css("height", `${przedmioty.length *2.5 + 2.5}rem`);
}

function Raporty_function() {
  console.log("Raporty_function");
  $("#informacje").empty();
  $("#zawartosc").empty();
  $("#zawartosc").html(`
    <label>Kto: <select id = 'kto_raport'>
      <option>-----------</option>
      <option>Nauczyciele</option>
      <option>Uczniowie</option>
    </select></label>
    <div id = 'formularz_raporty'>
      <div id = "pola_formularz_raporty"></div>
      <div id = "filtrowanie_formularz_raporty"></div>
    </div>
    <div id = 'raport_podglad'></div>
    <button id = "zapisz_raport">Zapisz plik pdf</button>
  `);

}

function Przedmioty_function() {
  console.log("Przedmioty_function");
  $("#informacje").empty();
  $("#zawartosc").empty();
  html = "<div class = 'form_element'><label>Skrót: <input type = 'text' id = 'skrot_przedmiotu_form'></label></div>" +
  "<div class = 'form_element'><label>Nazwa *: <input type = 'text' id = 'nazwa_przedmiotu_form' pattern='.{3,}'autofocus></label></div>" +
  "<div class = 'form_element'><button id = 'dodaj_przedmiot_form'>Dodaj</button></div>";

  przedmioty = pobierz_przedmioty_lista_function();
    console.log(przedmioty);
    if(przedmioty != null) {
      html += "<table>";
      html += "<tr><th>Skrót</th><th>Nazwa</th><th>Edytuj</th><th>Usuń</th></tr>";
      for(i = 0; i < przedmioty.length; i++) {
        html +=  "<tr><td><input  type = 'text' id = '" + przedmioty[i]["id"] + "_skrot' value =  '" + przedmioty[i]["skrot"]+ "'></input></td>"
        html += "<td><input type = 'text' id = '" + przedmioty[i]["id"] + "_nazwa' value =  '" + przedmioty[i]["nazwa"]+ "'></input></td>";
        html += "<td><button onClick = 'zmien_przedmiot_function(" + przedmioty[i]["id"] + ") Przedmioty_function();' id = '" + przedmioty[i]["id"] + "'>Zmień</button></td>";
        html += "<td><button onClick = 'usun_przedmiot_function(" + przedmioty[i]["id"] + "); Przedmioty_function();' id = '" + przedmioty[i]["id"] + "'>Usuń</button></td></tr>";
      }
    }
    $("#zawartosc").html(html);
}

function Szkoly_function() {
  console.log("Szkoly_function");
  $("#informacje").empty();
  $("#zawartosc").empty();
  html = `<div class = 'form_element'><label>Nazwa szkoły *: <input type = 'text' id = 'nazwa_szkoly_form' autofocus></label></div>
  <div class = 'form_element'><label>Liczba klas *: <input type = 'number' id = 'ilosc_szkoly_form''></label></div>
  <div class = 'form_element'><button onClick = 'dodaj_szkole_function(); Szkoly_function();'>Dodaj</button></div>`;
  szkoly = pobierz_szkoly_lista_function();
  if(szkoly != null) {
    html += `<div><div class = "kolumna_szkoly_form">Nazwa szkoły</div><div class = "kolumna_szkoly_form">Liczba klas</div><div class = "kolumna_szkoly_form">Edytuj</div><div class = "kolumna_szkoly_form">Usuń</div></div>`;
    for(i = 0; i < szkoly.length; i++) {
      html +=  `<div><div class = "kolumna_szkoly_form"><input type = 'text' id = '${szkoly[i]["id"]}_nazwa' value =  '${szkoly[i]["nazwa"]}'></input></div>
        <div class = "kolumna_szkoly_form"><input type = 'number' id = '${szkoly[i]["id"]}_liczba_klas' value =  '${szkoly[i]["liczba_klas"]}'></input></div>
        <div class = "kolumna_szkoly_form"><button onClick = 'zmien_szkola_klasy_function(${szkoly[i]["id"]}); Szkoly_function();'>Zmień</button></div>
        <div class = "kolumna_szkoly_form"><button onClick = 'usun_szkola_klasy_function(${szkoly[i]["id"]}); Szkoly_function();'>Usuń</button></div></div>`;
    }
  }
  $("#zawartosc").html(html);

}

function Uczniowie_function() {
  console.log("Uczniowie_function");
  $("#informacje").empty();
  $("#zawartosc").empty();
  $("#zawartosc").html("<button id = 'wyswietl_uczniowie_form'>Dodaj ucznia</button>");
  $("#zawartosc").append("<div><label>Szukaj <input title = 'Wpisz informację taką jak imie, nazawisko, miasto, numer telefonu' type = 'text' id = 'szukaj_uczniowie_lista'></label></div>");
  $("#zawartosc").append("<div><label>Kogo<select id = 'archiwum_uczniowie_lista'></select></label></div>");
  $("#archiwum_uczniowie_lista").append("<option value = 'wszyscy'>Wszyscy</option>");
  $("#archiwum_uczniowie_lista").append("<option value = 'uczen'>Uczniowie</option>");
  $("#archiwum_uczniowie_lista").append("<option value = 'archiwum'>Absolwęci</option>");
  $("#zawartosc").append("<div><label> Sortowanie<select id = 'sortowanie_uczniowie_lista'></select></label></div>");
  $("#sortowanie_uczniowie_lista").append("<option value = 'a->z'>[Nazwiska] Od A do Z</option>");
  $("#sortowanie_uczniowie_lista").append("<option value = 'z->a'>[Nazwiska] Od Z do A</option>");
  $("#sortowanie_uczniowie_lista").append("<option value = '1->n'>[Klasy] Rosnąco</option>");
  $("#sortowanie_uczniowie_lista").append("<option value = 'n->1'>[Klasy] Malejąco</option>");
  $("#zawartosc").append("<div id = 'form_uczniowie_div'></div>");
  $("#zawartosc").append("<div id = 'lista_uczniowie_div'></div>");
  pobierz_uczniowie_lista_function();

}

function uczniowie_form_function(edytuj_array) {
  console.log("uczniowie_form_function");
  if (!$('#form_uczniowie_div').is(':empty')){
    $("#form_uczniowie_div").empty();
    $("#lista_uczniowie_div").show();
    return;
  }

  if(typeof edytuj_array["id"] === 'undefined') {
    edytuj_array["zamieszkanie"] = {"miasto": "", "ulica": "", "kod": ""};
    edytuj_array["zameldowanie"] = {"miasto": "", "ulica": "", "kod": ""};
    edytuj_array["rodzic"] = {0: {"imie": "", "drugie_imie": "", "nazwisko" : "", "zameldowanie": {"miasto": "", "ulica": "", "kod": ""}, "zamieszkanie" : {"miasto": "", "ulica": "", "kod": ""}}, 1 : {"imie": "", "drugie_imie": "", "nazwisko" : "", "zameldowanie": {"miasto": "", "ulica": "", "kod": ""}, "zamieszkanie" : {"miasto": "", "ulica": "", "kod": ""}}};
  }

  $("#lista_uczniowie_div").hide();
  html = `<h1>Dodaj Ucznia</h1>
  <form action = '' id = 'uczniowie_form'><div class = 'formularz'><fieldset><legend style = 'font-size: 3rem;'>Dane ucznia</legend>
  <div class = 'form_element'><label>Imie *: <input value = '${edytuj_array["imie"] || ""}' data-type = 'imie/nazwisko' type = 'text' id = 'imie_uczniowie_form' autofocus required></label></div>
  <div class = 'form_element'><label>Drugie imie: <input value = '${edytuj_array["drugie_imie"] || ""}' data-type = 'imie/nazwisko' type = 'text' id = 'drugie_imie_uczniowie_form'></label></div>
  <div class = 'form_element'><label>Nazwisko *: <input value = '${edytuj_array["nazwisko"] || ""}' data-type = 'imie/nazwisko' type = 'text' id = 'nazwisko_uczniowie_form' required></label></div>
  <div class = 'form_element'><label>Pesel *: <input value = '${edytuj_array["pesel"] || ""}' data-type = 'pesel' type = 'text' id = 'pesel_uczniowie_form' required></label></div>
  <div class = 'form_element'><label>Data urodzenia *: <input value = '${edytuj_array["data_urodzenia"] || ""}' data-type = 'data' required type = 'date' id = 'data_urodzenia_uczniowie_form' value = '1995-01-01'></label></div>";
  <div class = 'form_element'><label>Miejsce urodzenia *: <input value = '${edytuj_array["miejsce_urodzenia"] || ""}' data-type = 'miejsce' required type = 'text' id = 'miejsce_urodzenia_uczniowie_form'></label></div>
  <div class = 'form_element'><label>Ważność orzeczenia lekarskiego *: <input value = '${edytuj_array["data_orzeczenie"] || ""}'data-type = 'data' required type = 'date' id = 'data_orzeczenia_uczniowie_form' value = '2019-01-01'></label></div>
  <div class = 'form_element'><label>Samodzielne poruszanie się *: <input required type = 'checkbox' id = 'samodzielne_poruszanie_uczniowie_form' checked></label></div>

  <fieldset><legend>Zamieszkanie</legend>
  <div class = 'form_element'><label>Miasto *: <input value = '${edytuj_array["zamieszkanie"]["miasto"] || ""}' data-type = 'miejsce' required type = 'text' id = 'miasto_zamieszkania_uczniowie_form'></label></div>
  <div class = 'form_element'><label>Ulica *: <input value = '${edytuj_array["zamieszkanie"]["ulica"] || ""}' data-type = 'ulica' required type = 'text' id = 'ulica_zamieszkania_uczniowie_form'></label></div>
  <div class = 'form_element'><label>Kod pocztowy *: <input value = '${edytuj_array["zamieszkanie"]["kod"] || ""}' data-type = 'kod_pocztowy' required type = 'text' id = 'kod_pocztowy_zamieszkania_uczniowie_form' placeholder = 'xx-xxx'></label></div></fieldset>

  <fieldset><legend>Zameldowanie</legend>
  <div class = 'form_element'><label>Miasto *: <input value = '${edytuj_array["zameldowanie"]["miasto"] || ""}' data-type = 'miejsce' required type = 'text' id = 'miasto_zameldowania_uczniowie_form'></label></div>
  <div class = 'form_element'><label>Ulica *: <input value = '${edytuj_array["zameldowanie"]["ulica"] || ""}' data-type = 'ulica' required type = 'text' id = 'ulica_zameldowania_uczniowie_form'></label></div>
  <div class = 'form_element'><label>Kod pocztowy *: <input value = '${edytuj_array["zameldowanie"]["kod"] || ""}' data-type = 'kod_pocztowy' required type = 'text' id = 'kod_pocztowy_zameldowania_uczniowie_form' placeholder = 'xx-xxx' ></label></div></fieldset>

  <fieldset><legend>Kontakt</legend>";
  <div class = 'form_element'><label>Telefon *: <input value = '${edytuj_array["telefon"]|| ""}' placeholder = '123-456-789'  data-type = 'komorka' required type = 'tel' id = 'telefon_uczniowie_form'></label></div>
  <div class = 'form_element'><label>Telefon Stacjonarny: <input value = '${edytuj_array["telefon_stacjonarny"] || ""}' placeholder = '123-45-79'  data-type = 'stacjonarny'  type = 'tel' id = 'telefon_stacjonarny_uczniowie_form'></label></div>
  <div class = 'form_element'><label>Email *: <input required value = '${edytuj_array["email"] || ""}' type = 'email' id = 'email_uczniowie_form'></label></div></fieldset>

  <fieldset><legend>Szkoła</legend>
  <div class = 'form_element'><label>Szkoła: <select id = 'szkola_uczniowie_form' onChange = 'liczba_klas_form_function()'>
  <option defoult>---------</option>`;
  szkoly = Array();
  $.ajax({
    async: false,
    url: "Odczytaj/szkoly_klasy.php",
    method: "post"
  })
  .done(res => {
    szkoly = JSON.parse(res);
  })
  .fail(error => {
    console.log(error);
  });
  if(szkoly.length == null) {
    $("#informacje").html("Zanim dodasz ucznia musisz dodać szkołę do której będzie przypisany");
    $("#Szkoly").click();
  }
  for(i = 0; i < szkoly.length; i++)
    html += "<option data-liczba_klas = '" + szkoly[i]["liczba_klas"] + "'>" + szkoly[i]["nazwa"] + "</option>";
  html += `</select></label></div>
  <div class = 'form_element'><label>Klasa:<select id = 'klasa_uczniowie_form' disabled>
  <option defoult>---------</option>
  </select></label></div></fieldset></fieldset>

  <fieldset><legend style = 'font-size: 3rem;'>Dane rodzica</legend>
  <div class = 'form_element'><label>Imie *: <input value = '${edytuj_array["rodzic"][0]["imie"] || ""}' required data-type = 'imie/nazwisko' type = 'text' id = 'imie_rodzic1_form'></label></div>
  <div class = 'form_element'><label>Drugie imie : <input value = '${edytuj_array["rodzic"][0]["drugie_imie"] || ""}'  data-type = 'imie/nazwisko' type = 'text' id = 'drugie_imie_rodzic1_form'></label></div>
  <div class = 'form_element'><label>Nazwisko *: <input value =  '${edytuj_array["rodzic"][0]["nazwisko"] || ""}' required data-type = 'imie/nazwisko' type = 'text' id = 'nazwisko_rodzic1_form'></label></div>
  <fieldset><legend>Zamieszkanie</legend>
    <div class = 'form_element'><label>Miasto *: <input value = '${edytuj_array["rodzic"][0]["zamieszkanie"]["miasto"] || ""}'  required data-type = 'miejsce' type = 'text' id = 'miasto_zamieszkania_rodzic1_form'></label></div>
    <div class = 'form_element'><label>Ulica *: <input value = '${edytuj_array["rodzic"][0]["zamieszkanie"]["ulica"] || ""}'  required data-type = 'miejsce' type = 'text' id = 'ulica_zamieszkania_rodzic1_form'></label></div>
    <div class = 'form_element'><label>Kod pocztowy *:<input value = '${edytuj_array["rodzic"][0]["zamieszkanie"]["kod"] || ""}' required placeholder = 'xx-xxx' data-type = 'kod_pocztowy' type = 'text' id = 'kod_pocztowy_zamieszkania_rodzic1_form'></label></div>
  </fieldset>
  <fieldset><legend>Zameldowanie</legend>
    <div class = 'form_element'><label>Miasto *:<input value = '${edytuj_array["rodzic"][0]["zameldowanie"]["miasto"] || ""}'  required data-type = 'miejsce' type = 'text' id = 'miasto_zameldowania_rodzic1_form'></label></div>
    <div class = 'form_element'><label>Ulica *:<input value = '${edytuj_array["rodzic"][0]["zameldowanie"]["ulica"] || ""}' required data-type = 'miejsce' type = 'text' id = 'ulica_zameldowania_rodzic1_form'></label></div>
    <div class = 'form_element'><label>Kod pocztowy *:<input value = '${edytuj_array["rodzic"][0]["zameldowanie"]["kod"] || ""}' required placeholder = 'xx-xxx' data-type = 'kod_pocztowy' type = 'text' id = 'kod_pocztowy_zameldowania_rodzic1_form'></label></div>
  </fieldset>
  <fieldset><legend>Kontakt</legend>
    <div class = 'form_element'><label>Telefon *: <input value = '${edytuj_array["rodzic"][0]["telefon"] || ""}' required placeholder = '123-456-789'  data-type = 'komorka' required type = 'tel' id = 'telefon_rodzic1_form'></label></div>
    <div class = 'form_element'><label>Telefon Stacjonarny: <input value = '${edytuj_array["rodzic"][0]["telefon_stacjonarny"] || ""}'  placeholder = '123-45-79'  data-type = 'stacjonarny'  type = 'tel' id = 'telefon_stacjonarny_rodzic1_form'></label></div>
    <div class = 'form_element'><label>Email *: <input value = '${edytuj_array["rodzic"][0]["email"] || ""}'  required type = 'email' id = 'email_rodzic1_form'></label></div>
    </fieldset>
  </fieldset>

  <fieldset><legend style = 'font-size: 3rem;'>Dane rodzica</legend>
    <div class = 'form_element'><label>Imie *: <input value = '${edytuj_array["rodzic"][1]["imie"] || ""}' data-type = 'imie/nazwisko' type = 'text' id = 'imie_rodzic2_form'></label></div>
    <div class = 'form_element'><label>Drugie imie : <input value = '${edytuj_array["rodzic"][1]["drugie_imie"] || ""}' data-type = 'imie/nazwisko' type = 'text' id = 'drugie_imie_rodzic2_form'></label></div>
    <div class = 'form_element'><label>Nazwisko *: <input value =  '${edytuj_array["rodzic"][1]["nazwisko"] || ""}' data-type = 'imie/nazwisko' type = 'text' id = 'nazwisko_rodzic2_form'></label></div>
    <fieldset><legend>Zamieszkanie</legend>
      <div class = 'form_element'><label>Miasto *: <input value = '${edytuj_array["rodzic"][1]["zamieszkanie"]["miasto"] || ""}' data-type = 'miejsce' type = 'text' id = 'miasto_zamieszkania_rodzic2_form'></label></div>
      <div class = 'form_element'><label>Ulica *: <input value = '${edytuj_array["rodzic"][0]["zamieszkanie"]["ulica"] || ""}'  data-type = 'miejsce' type = 'text' id = 'ulica_zamieszkania_rodzic2_form'></label></div>
      <div class = 'form_element'><label>Kod pocztowy *:<input value = '${edytuj_array["rodzic"][0]["zamieszkanie"]["kod"] || ""}' placeholder = 'xx-xxx' data-type = 'kod_pocztowy' type = 'text' id = 'kod_pocztowy_zamieszkania_rodzic2_form'></label></div>
    </fieldset>
    <fieldset><legend>Zameldowanie</legend>
      <div class = 'form_element'><label>Miasto *:<input value = '${edytuj_array["rodzic"][1]["zameldowanie"]["miasto"] || ""}' data-type = 'miejsce' type = 'text' id = 'miasto_zameldowania_rodzic2_form'></label></div>
      <div class = 'form_element'><label>Ulica *:<input value = '${edytuj_array["rodzic"][1]["zameldowanie"]["ulica"] || ""}' data-type = 'miejsce' type = 'text' id = 'ulica_zameldowania_rodzic2_form'></label></div>
      <div class = 'form_element'><label>Kod pocztowy *:<input value = '${edytuj_array["rodzic"][0]["zameldowanie"]["kod"] || ""}' placeholder = 'xx-xxx' data-type = 'kod_pocztowy' type = 'text' id = 'kod_pocztowy_zameldowania_rodzic2_form'></label></div>
    </fieldset>
    <fieldset><legend>Kontakt</legend>
      <div class = 'form_element'><label>Telefon *: <input value = '${edytuj_array["rodzic"][1]["telefon"] || ""}' placeholder = '123-456-789'  data-type = 'komorka' required type = 'tel' id = 'telefon_rodzic2_form'></label></div>
      <div class = 'form_element'><label>Telefon Stacjonarny: <input  value = '${edytuj_array["rodzic"][0]["telefon_stacjonarny"] || ""}' placeholder = '123-45-79'  data-type = 'stacjonarny'  type = 'tel' id = 'telefon_stacjonarny_rodzic2_form'></label></div>
      <div class = 'form_element'><label>Email *: <input value = '${edytuj_array["rodzic"][0]["email"] || ""}' type = 'email' id = 'email_rodzic2_form'></label></div></fieldset>
    </fieldset>

  <div class = 'form_element'><button data-id = '${edytuj_array["id"]}' id = 'zapisz_uczniowie_form'>Dodaj</button></div></form>`;
  $("#form_uczniowie_div").html(html);
  if(edytuj_array !== 'undefined') {
    $("#zapisz_uczniowie_form").html("Edytuj");
    $("#zapisz_uczniowie_form").attr("id", `edytuj_uczniowie_button`);

    $("#szkola_uczniowie_form > option").each(function() {
      if($(this).text() == edytuj_array["szkola"])
        $(this).attr("selected", true)
    });

    $("#szkola_uczniowie_form").change();
    $("#klasa_uczniowie_form > option").each(function() {
      if($(this).text() == edytuj_array["klasa"])
        $(this).attr("selected", true);

    });
    $("#klasa_uczniowie_form").attr("disabled", false);
    if(edytuj_array["samodzielne_poruszanie"] == "false")
      $("#samodzielne_poruszanie_uczniowie_form").prop("checked", false);
  }
}

function liczba_klas_form_function(_this) {
    console.log("liczba_klas_form_function");
  $("#klasa_uczniowie_form").empty();
  for(i = 1; i <= $("#szkola_uczniowie_form option:selected").attr("data-liczba_klas"); i++)
    $("#klasa_uczniowie_form").append("<option>" + i + "</option>");
  $("#klasa_uczniowie_form").prop("disabled", false);
}

function wyswietl_uczniowie_lista_function(uczniowie) {
  console.log("wyswietl_uczniowie_form");
  $("#lista_uczniowie_div").empty();
  for(i = 0; i < uczniowie.length; i++) {
    osoba_div = `<div class = 'osoba_lista'>
    <div class = 'uczen_dane'>${uczniowie[i]["imie"]}  ${uczniowie[i]["nazwisko"]} Szkola: ${uczniowie[i]["szkola"]} klasa ${uczniowie[i]["klasa"]}</div>
    <div id = '${uczniowie[i]["id"]}' class = 'uczen_button uczen_button_edytuj'>Edytuj</div>
    <div data-operacja = 'dane' id = '${uczniowie[i]["id"]}' class = 'uczen_button uczen_button_dane'>Dane</div>
    <div data-operacja = 'kontakt' id = '${uczniowie[i]["id"]}' class = 'uczen_button'>Kontakt</div>
    <div data-operacja = 'rodzice' id = '${uczniowie[i]["id"]}' class = 'uczen_button rodzice_uczen_button'>Rodzice</div>
    <div data-operacja = 'zamieszkanie' id = '${uczniowie[i]["id"]}' class = 'uczen_button'>Zamieszkanie</div>
    <div data-operacja = 'zameldowanie' id = '${uczniowie[i]["id"]}' class = 'uczen_button'>Zameldowanie</div>
    <div  id = '${uczniowie[i]["id"]}' class = 'uczen_button uczen_button_archiwizuj'>Archiwizuj</div>
    <div id = '${uczniowie[i]['id']}_wyswietl' data-wyswietlane = '' class = 'uczniowie_wyswietl' hidden></div></div>`;
    $("#lista_uczniowie_div").append(osoba_div);
  }
}

function wyswietl_nauczyciele_lista_function(nauczyciele) {
  console.log("wyswietl_nauczyciele_form");
  $("#lista_nauczyciele_div ").empty();
  for(i = 0; i < nauczyciele.length; i++) {
    osoba_div = `<div class = 'osoba_lista'>
    <div class = 'uczen_dane'>${nauczyciele[i]["imie"]}  ${nauczyciele[i]["nazwisko"]}</div>
    <div id = '${nauczyciele[i]["id"]}' class = 'nauczyciel_button nauczyciel_button_edytuj'>Edytuj</div>
    <div data-operacja = 'dane' id = '${nauczyciele[i]["id"]}' class = 'nauczyciel_button nauczyciel_button_dane'>Dane</div>
    <div data-operacja = 'kontakt' id = '${nauczyciele[i]["id"]}' class = 'nauczyciel_button'>Kontakt</div>
    <div data-operacja = 'zamieszkanie' id = '${nauczyciele[i]["id"]}' class = 'nauczyciel_button'>Zamieszkanie</div>
    <div data-operacja = 'zameldowanie' id = '${nauczyciele[i]["id"]}' class = 'nauczyciel_button'>Zameldowanie</div>
    <div data-operacja = 'przedmioty' id = '${nauczyciele[i]["id"]}' class = 'nauczyciel_button'>Przedmioty</div>
    <div data-operacja = 'studia' id = '${nauczyciele[i]["id"]}' class = 'nauczyciel_button'>Studia</div>
    <div  id = '${nauczyciele[i]["id"]}' class = 'nauczyciel_button nauczyciel_button_archiwizuj'>Archiwizuj</div>
    <div id = '${nauczyciele[i]['id']}_wyswietl' data-wyswietlane = '' class = 'uczniowie_wyswietl' hidden></div></div>`;
    $("#lista_nauczyciele_div").append(osoba_div);
  }
}

function wyswietl_dane_nauczyciel_function(x,y) {
  console.log("wyswietl_dane_nauczyciel_function");
  for(i = 0; i < nauczyciele.length; i++) {
    if(nauczyciele[i]["id"] == x) {
      nauczyciel = nauczyciele[i];
      break;
    }
  }
  switch (y) {
    case 'dane':
      div = `<div>Imie: ${nauczyciel["imie"]}</div>
        <div>Drugie imie: ${nauczyciel["drugie_imie"]}</div>
        <div>Nazwisko: ${nauczyciel["nazwisko"]}</div>
        <div>Pesel: ${nauczyciel["pesel"]}</div>
        <div>Rok urodzenia: ${nauczyciel["rok_urodzenia"]}</div>`;
      if(nauczyciel["archiwum"] == "true") div += `<div>Archiwum: Tak</div>`;
      else div += `<div>Archiwum: Nie</div>`;
      $(`#${x}_wyswietl`).css("height", `${6*2.2}rem`);
      $(`#${x}_wyswietl`).html(div);
      break;
    case 'kontakt':
      div = `<div>Telefon: ${nauczyciel["kontakt"]["telefon"]}</div>
        <div>Telefon Stacjonarny: ${nauczyciel["kontakt"]["telefon_stacjonarny"]} </div>
        <div>Email: ${nauczyciel["kontakt"]["email"]}</div>`;
      $(`#${x}_wyswietl`).css("height", `${3*2.2}rem`);
      $(`#${x}_wyswietl`).html(div);
      break;
    case 'zamieszkanie':
      gdzie = nauczyciel["zamieszkanie"];
      div = `<div>Miasto: ${gdzie["miasto"]}</div>
        <div>Ulica: ${gdzie["ulica"]}</div>
        <div>Kod pocztowy: ${gdzie["kod"]}</div>`;
      $(`#${x}_wyswietl`).css("height", `${3*2.2}rem`);
      $(`#${x}_wyswietl`).html(div);
      break;
    case 'zameldowanie':
      gdzie = nauczyciel["zameldowanie"];
      div = `<div>Miasto: ${gdzie["miasto"]}</div>
        <div>Ulica: ${gdzie["ulica"]}</div>
        <div>Kod pocztowy: ${gdzie["kod"]}</div>`;
      $(`#${x}_wyswietl`).css("height", `${3*2.2}rem`);
      $(`#${x}_wyswietl`).html(div);
      break;
    case 'przedmioty' :
      div = ``;
      nauczyciel["przedmioty"].forEach((przedmiot) => {
        div += `<div>${przedmiot}</div>`;
      });
      $(`#${x}_wyswietl`).css("height", `${nauczyciel["przedmioty"].length *2.2}rem`);
      $(`#${x}_wyswietl`).html(div);
      break;
    case 'studia':
    div = '';
    nauczyciel["studia"].forEach((studia) => {
      let element = `<div class = 'studia_nauczyciele_lista'><div>Uczelnia: ${studia['uczelnia']}</div>
      <div>Kierunek: ${studia['kierunek']}</div>
      <div>Data ukończenia: ${studia['data_ukonczenia']}</div>
      <div>Stopień: ${studia['stopien']}</div>
      </div>`;
      div += element;
    });
    $(`#${x}_wyswietl`).css("height", `${nauczyciel["studia"].length *2.2 * 4}rem`);
    $(`#${x}_wyswietl`).html(div);
    break;
  }
}

function wyswietl_dane_uczen_function(x,y) {
  console.log("wyswietl_dane_uczen_function");
  for(i = 0; i < uczniowie.length; i++) {
    if(uczniowie[i]["id"] == x) {
      uczen = uczniowie[i];
      break;
    }
  }
  switch (y) {
    case 'dane':
      div = `<div>Imie: ${uczen["imie"]}</div>
        <div>Drugie imie: ${uczen["drugie_imie"]}</div>
        <div>Nazwisko: ${uczen["nazwisko"]}</div>
        <div>Pesel: ${uczen["pesel"]}</div>
        <div>Data urodzenia: ${uczen["data_urodzenia"]}</div>
        <div>Miejsce urodzenia: ${uczen["miejsce_urodzenia"]}</div>
        <div>samodzielne_poruszanie: ${uczen["samodzielne_poruszanie"]}</div>
        <div>Ważność orzeczenia: ${uczen["data_orzeczenie"]}</div>`;
      $(`#${x}_wyswietl`).css("height", `${8*2.2}rem`);
      $(`#${x}_wyswietl`).html(div);
      break;
    case 'kontakt':
      div = `<div>Telefon: ${uczen["telefon"]}</div>
        <div>Telefon Stacjonarny: ${uczen["telefon_stacjonarny"]} </div>
        <div>Email: ${uczen["email"]}</div>`;
      $(`#${x}_wyswietl`).css("height", `${3*2.2}rem`);
      $(`#${x}_wyswietl`).html(div);
      break;
    case 'zamieszkanie':
      gdzie = uczen["zamieszkanie"];
      div = `<div>Miasto: ${gdzie["miasto"]}</div>
        <div>Ulica: ${gdzie["ulica"]}</div>
        <div>Kod pocztowy: ${gdzie["kod"]}</div>`;
      $(`#${x}_wyswietl`).css("height", `${3*2.2}rem`);
      $(`#${x}_wyswietl`).html(div);
      break;
    case 'zameldowanie':
      gdzie = uczen["zameldowanie"];
      div = `<div>Miasto: ${gdzie["miasto"]}</div>
        <div>Ulica: ${gdzie["ulica"]}</div>
        <div>Kod pocztowy: ${gdzie["kod"]}</div>`;
      $(`#${x}_wyswietl`).css("height", `${3*2.2}rem`);
      $(`#${x}_wyswietl`).html(div);
      break;
    case 'rodzice':
      rodzic = uczen["rodzic"][0];
      console.log(rodzic);
      div_1 = `<div class = 'rodzic'>
        <div>Imie: ${rodzic["imie"]}</div>
        <div>Drugie imie: ${rodzic["drugie_imie"]}</div>
        <div>Nazwisko: ${rodzic["nazwisko"]}</div>
        <div>telefon: ${rodzic["telefon"]}</div>
        <div>telefon_stacjonarny: ${rodzic["telefon_stacjonarny"]}</div>
        <div>Miasto zamieszkania: ${rodzic["zamieszkanie"]["miasto"]}</div>
        <div>Ulica zamieszkania: ${rodzic["zamieszkanie"]["ulica"]}</div>
        <div>Kod pocztowy zamieszkania: ${rodzic["zamieszkanie"]["kod"]}</div>
        <div>Miasto zameldowania: ${rodzic["zameldowanie"]["miasto"]}</div>
        <div>Ulica zameldowania: ${rodzic["zameldowanie"]["ulica"]}</div>
        <div>Kod pocztowy zameldowania: ${rodzic["zameldowanie"]["kod"]}</div>
      </div>`;
      rodzic = uczen["rodzic"][1];
      if(typeof rodzic !== 'undefined')
        div_2 = `<div class = 'rodzic'>
          <div>Imie: ${rodzic["imie"]}</div>
          <div>Drugie imie: ${rodzic["drugie_imie"]}</div>
          <div>Nazwisko: ${rodzic["nazwisko"]}</div>
          <div>telefon: ${rodzic["telefon"]}</div>
          <div>telefon_stacjonarny: ${rodzic["telefon_stacjonarny"]}</div>
          <div>Miasto zamieszkania: ${rodzic["zamieszkanie"]["miasto"]}</div>
          <div>Ulica zamieszkania: ${rodzic["zamieszkanie"]["ulica"]}</div>
          <div>Kod pocztowy zamieszkania: ${rodzic["zamieszkanie"]["kod"]}</div>
          <div>Miasto zameldowania: ${rodzic["zameldowanie"]["miasto"]}</div>
          <div>Ulica zameldowania: ${rodzic["zameldowanie"]["ulica"]}</div>
          <div>Kod pocztowy zameldowania: ${rodzic["zameldowanie"]["kod"]}</div>
        </div>`;

      $(`#${x}_wyswietl`).css("height", `${8*2.2}rem`);
      $(`#${x}_wyswietl`).html(div_1 + div_2);
      break;
  }
}

function edytuj_uczen_form_function() {
  console.log("edytuj_uczen_form_function");
  id = $(this).attr("id");
  for(i = 0; i < uczniowie.length; i++)
    if(uczniowie[i]["id"] == id){
      uczniowie_form_function(uczniowie[i]);
      break;
    }
}

function edytuj_nauczyciel_form_function() {
  console.log("edytuj_nauczyciel_form_function");
  console.log(nauczyciele);
  id = $(this).attr("id");
  for(i = 0; i < nauczyciele.length; i++)
    if(nauczyciele[i]["id"] == id){
      nauczyciel_form_function(nauczyciele[i]);
      break;
    }
}
